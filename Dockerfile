FROM node:13.12.0-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
RUN npm audit fix
COPY . .
RUN npm run build
FROM nginx:1.17.1-alpine
COPY --from=build /usr/src/app/dist/ /usr/share/nginx/html
COPY /usr/share/nginx/html/default.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/default.conf


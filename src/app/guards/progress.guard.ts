import { Injectable } from "@angular/core";
import {
    CanDeactivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";

import { IProgress } from "../interfaces";

@Injectable({
    providedIn: "root",
})
export class ProgressGuard implements CanDeactivate<IProgress> {
    canDeactivate(
        component: IProgress,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState?: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (component.isProgress) {
            return confirm("Are you sure you want to leave this page ?");
        }

        return true;
    }
}

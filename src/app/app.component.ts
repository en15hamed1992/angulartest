import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import {
  NbLayoutDirection,
  NbLayoutDirectionService,
  NbThemeService,
} from "@nebular/theme";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "ngx-app",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  constructor(
    private http: HttpClient,
    private router: Router,
    private themeService: NbThemeService,
    private directionService: NbLayoutDirectionService
  ) {}

  ngOnInit(): void {
    const acl = localStorage.getItem("acl");
    const role = localStorage.getItem("role");
    const theme = localStorage.getItem("nb_theme") || "default";
    const direction = localStorage.getItem("nb_direction") || "ltr";

    if (!acl || !role) {
      // this.router.navigateByUrl("/auth/login");
    }

    this.themeService.changeTheme(theme);
    this.directionService.setDirection(
      direction === "rtl" ? NbLayoutDirection.RTL : NbLayoutDirection.LTR
    );

    this.themeService.onThemeChange().subscribe((theme: any) => {
      localStorage.setItem("nb_theme", theme.name);
    });

    this.directionService
      .onDirectionChange()
      .subscribe((direction: NbLayoutDirection) => {
        localStorage.setItem("nb_direction", direction.toString());
      });
  }
}

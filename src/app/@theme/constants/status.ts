export enum Status {
  PRIMARY = "#3366ff",
  SUCCESS = "#00d68f",
  INFO = "#0095ff",
  WARNING = "#ffaa00",
  DANGER = "#ff3d71",
  PRIMARY_TRANSPARENT = "rgba(51, 102, 255, 0.32)",
  SUCCESS_TRANSPARENT = "rgba(0, 214, 143, 0.32)",
  INFO_TRANSPARENT = "rgba(0, 149, 255, 0.32)",
  WARNING_TRANSPARENT = "rgba(255, 170, 0, 0.32)",
  DANGER_TRANSPARENT = "rgba(255, 61, 113, 0.32)",
}

import { Component, OnDestroy, OnInit } from "@angular/core";
import {
    NbMediaBreakpointsService,
    NbMenuService,
    NbSidebarService,
    NbThemeService,
    NbLayoutDirectionService,
    NbLayoutDirection,
} from "@nebular/theme";

import { UserData } from "../../../@core/data/users";
import { LayoutService } from "../../../@core/utils";
import { map, takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { UserService } from "../../../@core/mock/users.service";
import { AuthService } from "../../../custom-auth/services/auth.service";

@Component({
    selector: "ngx-header",
    styleUrls: ["./header.component.scss"],
    templateUrl: "./header.component.html",
})
export class HeaderComponent implements OnInit, OnDestroy {
    private destroy$: Subject<void> = new Subject<void>();
    userPictureOnly: boolean = false;
    user: any;

    themes: any[];

    currentTheme = "default";

    languageToggle: boolean;

    constructor(
        private sidebarService: NbSidebarService,
        private menuService: NbMenuService,
        private themeService: NbThemeService,
        private authService: AuthService,
        private layoutService: LayoutService,
        private breakpointService: NbMediaBreakpointsService,
        private directionService: NbLayoutDirectionService
    ) {}

    ngOnInit() {
        this.currentTheme = this.themeService.currentTheme;
        this.themes = [
            {
                value: "default",
                name: "Light",
            },
            {
                value: "dark",
                name: "Dark",
            },
            {
                value: "cosmic",
                name: "Cosmic",
            },
            {
                value: "corporate",
                name: "Corporate",
            },
        ];

        this.authService.getCurrentUser().subscribe((user: any) => {
            this.user = {
                ...user,
                picture: "https://www.shareicon.net/data/256x256/2016/05/26/771188_man_512x512.png",
            };
        });

        const { xl } = this.breakpointService.getBreakpointsMap();
        this.themeService
            .onMediaQueryChange()
            .pipe(
                map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
                takeUntil(this.destroy$)
            )
            .subscribe((isLessThanXl: boolean) => (this.userPictureOnly = isLessThanXl));

        this.themeService
            .onThemeChange()
            .pipe(
                map(({ name }) => name),
                takeUntil(this.destroy$)
            )
            .subscribe((themeName) => (this.currentTheme = themeName));

        this.directionService.onDirectionChange().subscribe((direction: NbLayoutDirection) => {
            if (direction === NbLayoutDirection.LTR) {
                this.languageToggle = false;
            } else {
                this.languageToggle = true;
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    changeTheme(themeName: string) {
        this.themeService.changeTheme(themeName);
    }

    toggleSidebar(): boolean {
        this.sidebarService.toggle(true, "menu-sidebar");
        this.layoutService.changeLayoutSize();

        return false;
    }

    navigateHome(): void {
        this.menuService.navigateHome();
    }

    languageToggleChanged(languageToggleValue: boolean) {
        if (!languageToggleValue) {
            this.directionService.setDirection(NbLayoutDirection.LTR);
        } else {
            this.directionService.setDirection(NbLayoutDirection.RTL);
        }
    }
}


export interface PagerModel {
    currentPage: number;
    pageSize: number;
    pagesCount: number;
    recordsCount: number;
    hasPrevious: boolean;
    hasNext: boolean;
}

import { PagerModel } from "./pager.model";

export interface ResponseModel<TData> {
  success: boolean;

  result: TData | TData[];

  message?: string;

  erorrs?: any;

  pager: PagerModel;
}



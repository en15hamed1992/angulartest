import { Observable } from "rxjs";
import { ResponseModel } from "../models";

export interface ApiServiceInterface {
  getAll: (options?: { params?: any }) => Observable<ResponseModel<any>>;
}

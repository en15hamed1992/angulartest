import { NgModule } from "@angular/core";
import { ExtraOptions, RouterModule, Routes } from "@angular/router";

import { AuthGuard } from "./guards";

export const routes: Routes = [
    {
        path: "pages",
       // canActivate: [AuthGuard],
        loadChildren: () => import("./pages/pages.module").then((m) => m.PagesModule),
    },
    {
        path: "auth",
        loadChildren: () =>
            import("./custom-auth/custom-auth.module").then((m) => m.CustomAuthModule),
    },
    { path: "", redirectTo: "auth/login", pathMatch: "full" },
    { path: "**", redirectTo: "auth/login" },
];

const config: ExtraOptions = {
    useHash: false,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})
export class AppRoutingModule {}

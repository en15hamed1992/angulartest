import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { of } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class RoleProviderService {
    constructor(private router: Router) {}

    getRole() {
        if (localStorage.getItem("role")) {
            return of(localStorage.getItem("role"));
        } else {
            this.router.navigate(["auth/login"]);
        }
    }

    isGranted(permissions: string[], operator = "and") {
        return of(true);
        
        const roleAcl = JSON.parse(localStorage.getItem("acl"))[localStorage.getItem("role")];

        if (operator === "and") {
            return permissions.every((permission: any) => roleAcl.includes(permission));
        } else {
            let canAccess: boolean = false;

            permissions.forEach((value) => {
                if (roleAcl.includes(value)) {
                    canAccess = true;
                }
            });

            return of(canAccess);
        }
    }
}

import { LoadingService } from "./../../shared/loading-modal/loading-modal.service";
import { Component, OnInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { Status } from "../../@theme/constants/status";

import { AuthService } from "../services/auth.service";
import { finalize } from "rxjs/operators";

@Component({
  selector: "ngx-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    phoneNumber: new FormControl("", [
      Validators.required,
    ]),
    password: new FormControl("", [Validators.required]),
  });

  showError: boolean;
  showPassword: boolean;
  errorMsg: string;
  passValid: boolean;
  spinnerConfig = {
    bgColor: "rgba(0, 0, 0, 0.1)",
    color: Status.PRIMARY,
    type: "ball-clip-rotate",
    size: "large",
    fullScreen: true,
  };

  constructor(
    private router: Router,
    private loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    const nbSpinner = document.getElementById("nb-global-spinner");
    if (nbSpinner) {
      nbSpinner.style.display = "none";
    }
  }

  ValidatePassword(): string {
    if (this.loginForm.controls.password.value.length === 0) return "";

    return this.loginForm.controls.password.invalid === true
      ? "wrong-entry"
      : "success-entry";
  }

  checkifNumber(event: KeyboardEvent): boolean {
    const allowedKeys = "0123456789";
    return allowedKeys.includes(event.key) ? true : false;
  }

  getInputType() {
    if (this.showPassword) {
      return "text";
    }
    return "password";
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  onSubmit() {

    this.loadingService.openModal({ message: "Signing in..." });
    this.showError = false;
    this.errorMsg = "";
    if (this.loginForm.valid === true) {
            this.loadingService.closeModal();

      this.router.navigate(["pages"]);
    }
  }
}

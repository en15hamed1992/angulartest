import { environment } from "../../../environments/environment";
import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { delay, take } from "rxjs/operators";
import { FormControl } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector: "ngx-two-factor-auth",
    templateUrl: "./two-factor-auth.component.html",
    styleUrls: ["./two-factor-auth.component.scss"],
})
export class TwoFactorAuthComponent implements OnInit {
    imageSrc: string;
    verificationToken: FormControl = new FormControl("");

    constructor(private httpClient: HttpClient, private router: Router) {}

    ngOnInit(): void {
        const el = document.getElementById("nb-global-spinner");
        if (el) {
            el.style["display"] = "none";
        }
    }

    submitVerificationToken() {
        // TODO temprorily added this line to skip everything, please remove later
        this.router.navigateByUrl("/pages/dashboard");
        return;
        ////////////////////////////////

        const auth_app_token = JSON.parse(localStorage.getItem("auth_app_token"));

        const headers: any = {};
        if (auth_app_token && auth_app_token.apiKey) headers.apiKey = auth_app_token.apiKey;

        this.httpClient
            .put(
                `${environment.API_BASE_URL}/2fa/verify`,
                {
                    token: this.verificationToken.value,
                },
                { headers }
            )
            .pipe(take(1))
            .subscribe((res) => {
                localStorage.setItem(
                    "auth_app_token",
                    JSON.stringify({
                        ...auth_app_token,
                        "2fa_result": res,
                    })
                );

                if (res) {
                    this.router.navigateByUrl("/pages/dashboard");
                }
            });
    }
}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { Routes, RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NbButtonModule, NbInputModule, NbLayoutModule } from "@nebular/theme";

import { SharedModule } from "../shared/shared.module";
import { LoginComponent } from "./login/login.component";
import { TwoFactorAuthComponent } from "./two-factor-auth/two-factor-auth.component";
import { CustomAuthComponent } from "./custom-auth.component";

const routes: Routes = [
    {
        path: "",
        component: CustomAuthComponent,
        children: [
            {
                path: "login",
                component: LoginComponent,
            },
            {
                path: "2fa",
                component: TwoFactorAuthComponent,
            },
        ],
    },
];

@NgModule({
    declarations: [CustomAuthComponent, LoginComponent, TwoFactorAuthComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        SharedModule,
        ReactiveFormsModule,
        HttpClientModule,
        NbButtonModule,
        NbInputModule,
        NbLayoutModule,
    ],
    exports: [],
})
export class CustomAuthModule {}

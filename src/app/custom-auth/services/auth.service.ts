import { tokenCredentials } from './../../guards/credentials.model';
import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  JsonpClientBackend,
} from "@angular/common/http";
import { Observable, of, Subscriber, throwError } from "rxjs";
import {
  catchError,
  delay,
  finalize,
  map,
  mergeMap,
  tap,
} from "rxjs/operators";

import { environment } from "../../../environments/environment";
import { LoadingService } from "../../shared/loading-modal/loading-modal.service";
import { ErrorService } from "../../shared/error-modal/error-modal.service";
import { Credentials } from "./requests.model";
import { result } from "./reuslts.model";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private errorService: ErrorService
  ) {}

  login(credentials: Credentials): Observable<any> {
    return this.httpClient
      .post(`${environment.API_BASE_URL}${environment.login_URL}`, credentials)
      .pipe(
        tap((res: any) => {
          if (res?.success === true) {
            this.updateAppToken(res.result);
          }

          return res;
        })
      );
  }

  logout(): Observable<any> {
    this.loadingService.openModal({ message: "Logging out ..." });

    return new Observable((observer: Subscriber<void>) => {
      try {
        this.removeAppToken();
        observer.next();
        observer.complete();
      } catch {
        observer.error();
      }
    }).pipe(
      delay(2000),
      catchError((err: any) => {
        this.errorService.openModal({
          error: "Unknown Error",
          message: "Failed to logout",
        });

        return throwError(err);
      }),
      finalize(() => {
        this.loadingService.closeModal();
      })
    );
  }

  refreshToken(): Observable<any> {
    const credentials = this.getAppToken();

    return this.httpClient
      .post(
        `${environment.API_BASE_URL}${environment.refresh_Token_URL}`,
        credentials
      )
      .pipe(
        tap((res: any) => {
          if (res?.success === true) {
            this.updateAppToken(res.result);
          }

          return res;
        })
      );
  }

  getAccount(token: string): Observable<any> {
    return this.httpClient.get(
      `${environment.API_BASE_URL}${environment.account_URL}`
    );
  }

  getCurrentUser(): Observable<any> {
    return of({});
    // return this.httpClient.get(`${environment.API_BASE_URL}/account`, {
    //     headers: new HttpHeaders({ apiKey: apiKey }),
    // });
  }

  // Token stuff //

  getAppToken() {
    return JSON.parse(localStorage.getItem(environment.AUTH_APP_TOKEN_KEY));
  }

  removeAppToken() {
    localStorage.removeItem(environment.AUTH_APP_TOKEN_KEY);
  }

  updateAppToken(authTokenInfo: any) {
    if (authTokenInfo != null) {
      localStorage.setItem(
        environment.AUTH_APP_TOKEN_KEY,
        JSON.stringify(authTokenInfo)
      );
    }
  }
}

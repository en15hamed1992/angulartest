import { ContactsModule } from './admin-pages/contacts/contacts.module';
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { PagesComponent } from "./pages.component";
import { MainPageComponent } from "./admin-pages/main-page/main-page.component";
import { NotFoundPageComponent } from "./admin-pages/not-found-page/not-found-page.component";

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      {
        path: "dashboard",
        component: MainPageComponent,
      },
      {
        path: "contacts",
        loadChildren: () =>
          import("./admin-pages/contacts/contacts.module").then(
            (module) => module.ContactsModule
          ),
      },
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full",
      },
      {
        path: "**",
        component: NotFoundPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}

import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContactComponent } from './add-contatcts/add-contact/add-contact.component';
import { EditContactComponent } from './edit-contatcts/edit-contact/edit-contact.component';
import { ViewContactsComponent } from './view-contatcts/view-contacts/view-contacts.component';
import { Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { ViewFavorateContactsComponent } from './view-favorate-contacts/view-favorate-contacts.component';


const routes: Routes = [
  {
    path: "",
    component: ViewContactsComponent,
  },
  {
    path: "favorates",
    component: ViewFavorateContactsComponent,
  },
  {
    path: "edit/:id",
    component: EditContactComponent,
  },
  {
    path: "add",
    component: AddContactComponent,
  },
];
@NgModule({
  declarations: [AddContactComponent, EditContactComponent, ViewContactsComponent, ViewFavorateContactsComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), SharedModule
  ]
})
export class ContactsModule { }

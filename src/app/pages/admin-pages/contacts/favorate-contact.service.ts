import { ContactsService } from './contatcts.service';
import { ResponseModel } from './../../../models/response.model';
import { AddContact } from './models/add-contact.model';
import { Injectable } from '@angular/core';
import { Observable,of } from 'rxjs';
import { Contact, ContactDto } from './models/contact.model';
import { UpdateContact } from './models/update-contact.model';

@Injectable({
  providedIn: 'root'
})
export class FavorateContactsService {
  constructor(private contactService:ContactsService) {}
  
 
  getAll (options?: { params?: any; }):Observable<ResponseModel<Contact>>{
    
    return of({
      success: true,
    
      result: this.contactService.Contacts.filter(e=>e.isFavorate===true),
      pager: {
        currentPage: 1,
        pageSize: 10,
        pagesCount: 1,
        recordsCount:1,
        hasPrevious: false,
        hasNext: false,
    }
    });
  }

 
}

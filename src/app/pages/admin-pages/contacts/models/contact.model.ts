export interface Contact{
    id:number,
    name:string,
    phoneNumber:string;
    isFavorate:boolean
}
export class ContactDto implements Contact {
    constructor(
        public id:number,
        public name:string,
        public phoneNumber: string,
        public isFavorate:boolean=false
        ){}
}
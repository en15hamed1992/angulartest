import { DashboardTableColumns } from './../../../../../shared/dashboard-table/models/dashboard-table-columns.mode';
import { Component, OnInit } from '@angular/core';
import { DashboardTableAction, DashboardTableActions } from '../../../../../shared/dashboard-table/models/dashboard-table-action.model';
import { DashboardTableFilter } from '../../../../../shared/dashboard-table/models';
import { DashboardTableModifiers } from '../../../../../shared/dashboard-table/dashboard-table-modifiers';
import { NbToastRef, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { ConfirmationService } from '../../../../../shared/confirmation-modal/confirmation.service';
import { LoadingService } from '../../../../../shared/loading-modal/loading-modal.service';
import { AppKeywords, AppHelpers } from '../../../../../utils';
import { User } from '../../../user/models/User.model';
import { ContactsService } from '../../contatcts.service';
import { Contact } from '../../models/contact.model';

@Component({
  selector: 'ngx-view-contacts',
  templateUrl: './view-contacts.component.html',
  styleUrls: ['./view-contacts.component.scss']
})
export class ViewContactsComponent implements OnInit {
  columns: DashboardTableColumns;
  actions: DashboardTableAction[];
  filters: DashboardTableFilter[];
  sortFieldValueMap: { [key: string]: string };
  modifiers: DashboardTableModifiers;

  toast: NbToastRef;

  loading: boolean;
  constructor(
    private router: Router,
    private loadingService: LoadingService,
    private confirmationService: ConfirmationService,
    public conatctService: ContactsService
  ) {}
  ngOnDestroy(): void {
    this.confirmationService.closeModal();
    this.loadingService.closeModal();
  }
  ngOnInit(): void {
    this.initColumns();
    this.initActions();
    this.initFilters();
    this.initSortValueMap();

    this.loading = true;
  }

  initColumns(): void {
    this.columns = {
      id: {
        title: "id",
        type: "text",
        filter:false
      },
      name: {
        title: "name",
        type: "text",
        filter:false
      },
      phoneNumber: {
        title: "phone",
        type: "text",
        filter:false
      },
      isFavorate: {
        title: "isFavorate",
        type: "text",
        filter:false
       
      },
    };
  }

  initActions(): void {
    this.actions = [
      {
        action: DashboardTableActions.UPDATE,
      },
      {
        action: DashboardTableActions.VIEW_ONE,
      },

      {
        action: DashboardTableActions.CREATE,
      },
      {
        action: DashboardTableActions.DELETE,
      },
    ];
  }

  initFilters(): void {
    this.filters = [
      {
        columnId: "name",
        key: "name",
      },
    ];
  }

  initSortValueMap(): void {
    this.sortFieldValueMap = {
      name: "0",
      requiredScore: "1",
    };
  }

  onCreate(modifiers: DashboardTableModifiers): void {
    this.modifiers = modifiers;
  }

  onAdd(): void {
    this.router.navigate(["/pages/contacts/add"]);
  }
  onView(contact: Contact): void {
    this.router.navigate(["/pages/contacts/favorates"]);
  }

  onEdit(contact: Contact): void {
    this.router.navigate(["/pages/contacts/edit", contact.id]);
  }

  onDelete(contact: Contact) {
 

    const confirmkeyword = AppKeywords.ACTIVATE_KEYWORD.toLowerCase();
    const loadKeyword = AppKeywords.ACTIVATING_KEYWORD.toLowerCase();

    this.confirmationService.openModal({
      title: `confirm`,
      message: contact.isFavorate? `Are you sure you want to remove this contact from your favorate list?`:
      `Are you sure you want to add this contact to your favorate list?`,
      onConfirm: () => {
        this.conatctService.toggleFavorate(contact.id)
       this.resetFilters()
      },
    });
  }

  onLoad(): void {
    this.loading = false;
  }

  resetFilters(): void {
    this.modifiers.resetFilters();
  }
}

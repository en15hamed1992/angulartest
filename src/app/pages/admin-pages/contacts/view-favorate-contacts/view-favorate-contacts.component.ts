import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbToastRef } from '@nebular/theme';
import { ConfirmationService } from '../../../../shared/confirmation-modal/confirmation.service';
import { DashboardTableModifiers } from '../../../../shared/dashboard-table/dashboard-table-modifiers';
import { DashboardTableAction, DashboardTableColumns, DashboardTableFilter } from '../../../../shared/dashboard-table/models';
import { LoadingService } from '../../../../shared/loading-modal/loading-modal.service';
import { FavorateContactsService } from '../favorate-contact.service';

@Component({
  selector: 'ngx-view-favorate-contacts',
  templateUrl: './view-favorate-contacts.component.html',
  styleUrls: ['./view-favorate-contacts.component.scss']
})
export class ViewFavorateContactsComponent implements OnInit {

  columns: DashboardTableColumns;
  actions: DashboardTableAction[];
  filters: DashboardTableFilter[];
  sortFieldValueMap: { [key: string]: string };
  modifiers: DashboardTableModifiers;

  toast: NbToastRef;

  loading: boolean;
  constructor(
    private router: Router,
    private loadingService: LoadingService,
    private confirmationService: ConfirmationService,
    public favorateConatctService: FavorateContactsService
  ) {}
  ngOnDestroy(): void {
    this.confirmationService.closeModal();
    this.loadingService.closeModal();
  }
  ngOnInit(): void {
    this.initColumns();
    this.initActions();
    this.initFilters();
    this.initSortValueMap();

    this.loading = true;
  }

  initColumns(): void {
    this.columns = {
      
      name: {
        title: "name",
        type: "text",
        filter:false

      },
      phoneNumber: {
        title: "phone",
        type: "text",
        filter:false
      },
      isFavorate: {
        title: "isFavorate",
        type: "text",
        filter:false
       
      },
    };
  }

  initActions(): void {
    this.actions = [
     
    ];
  }

  initFilters(): void {
    
  }

  initSortValueMap(): void {
    this.sortFieldValueMap = {
      name: "0",
      requiredScore: "1",
    };
  }

  onCreate(modifiers: DashboardTableModifiers): void {
    this.modifiers = modifiers;
  }

  // onAdd(): void {
  //   this.router.navigate(["/pages/contacts/add"]);
  // }
  // onView(contact: Contact): void {
  //   this.conatctService.toggleFavorate
  //   this.router.navigate(["/pages/contacts/", contact.id]);
  // }

  // onEdit(contact: Contact): void {
  //   this.router.navigate(["/pages/contacts/edit", contact.id]);
  // }

  // onDelete(user: User) {
  //   const confirmkeyword = AppKeywords.ACTIVATE_KEYWORD.toLowerCase();
  //   const loadKeyword = AppKeywords.ACTIVATING_KEYWORD.toLowerCase();

  //   this.confirmationService.openModal({
  //     title: `${AppHelpers.uppercaseFirst(confirmkeyword)} User`,
  //     message: `Are you sure you want to ${confirmkeyword} User "${user.firstName}"  ${user.lastName}?`,
  //     onConfirm: () => {
  //       this.loadingService.openModal({
  //         message: `${loadKeyword} User "${user.firstName}"...`,
  //       });
  //     },
  //   });
  //}

  onLoad(): void {
    this.loading = false;
  }

  resetFilters(): void {
    this.modifiers.resetFilters();
  }
}

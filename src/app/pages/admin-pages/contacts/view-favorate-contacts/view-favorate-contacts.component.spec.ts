import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFavorateContactsComponent } from './view-favorate-contacts.component';

describe('ViewFavorateContactsComponent', () => {
  let component: ViewFavorateContactsComponent;
  let fixture: ComponentFixture<ViewFavorateContactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFavorateContactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFavorateContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbGlobalPhysicalPosition, NbToastRef, NbToastrService } from '@nebular/theme';
import { finalize } from 'rxjs/operators';
import { TextBoxTypes } from '../../../../../shared/dynamic-form/components/text-box/models';
import { DynamicTextBox } from '../../../../../shared/dynamic-form/items';
import { DynamicForm } from '../../../../../shared/dynamic-form/models';
import { LoadingService } from '../../../../../shared/loading-modal/loading-modal.service';
import { ContactsService } from '../../contatcts.service';
import { AddContact } from '../../models/add-contact.model';

@Component({
  selector: 'ngx-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {
  dynamicForm: DynamicForm;
  submitButtonTextContent: string;

  toast: NbToastRef;

  constructor(
    private router: Router,
    private nbToastrService: NbToastrService,
    private loadingService: LoadingService,
    private contactService: ContactsService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.submitButtonTextContent = "create_user";

    this.dynamicForm = new DynamicForm({
      items: {
        name: new DynamicTextBox({
          label: "name",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "Enter Name",
          },
        }),
        phoneNumber: new DynamicTextBox({
          label: "phone number",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter phone number",
          },
        }),
      }
    });
  }

  onSubmit(dfValues: AddContact): void {
    this.loadingService.openModal({ message: "Creating user..." });
    this.contactService
    .Add(dfValues)
    .pipe(
      finalize(() => {
        this.loadingService.closeModal();
      })
    )
    .subscribe(() => {
      this.toast = this.nbToastrService.show(
        "Contact has been created successfully",
        "Add Contact",
        {
          status: "success",
          hasIcon: true,
          icon: "checkmark-circle-2-outline",
          position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
        }
      );

      this.router.navigate(["/pages/contacts/"]);
    });
   
   
  }
}

import { ResponseModel } from './../../../models/response.model';
import { AddContact } from './models/add-contact.model';
import { Injectable } from '@angular/core';
import { Observable,of } from 'rxjs';
import { Contact, ContactDto } from './models/contact.model';
import { UpdateContact } from './models/update-contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  constructor() {}
  
 private contacts:Contact[]=[
   {
     id:1,
     name:'conact',
     phoneNumber:'123',
     isFavorate:false
   }
 ]
 
 public get Contacts() : Contact[] {
   return this.contacts;
 }
 
  getAll (options?: { params?: any; }):Observable<ResponseModel<Contact>>{
    return of({
      success: true,
    
      result: this.contacts,
      pager: {
        currentPage: 1,
        pageSize: 10,
        pagesCount: 1,
        recordsCount:1,
        hasPrevious: false,
        hasNext: false,
    }
    });
  }

  getById(id:number): Observable<ResponseModel<Contact>>{
   const contact=this.getContact(id);
    return of({
      success: true,
    
      result: contact,
      pager: {
        currentPage: 1,
        pageSize: 10,
        pagesCount: 1,
        recordsCount:1,
        hasPrevious: false,
        hasNext: false,
    }
    });
  }


  private getContact(id:number):Contact{
    const contact=this.contacts.find(e=>e.id===id)
    if(!contact)throw new Error("not found");
    return contact;
  }


  Add(model:AddContact) : Observable<Contact>{
    const conactId=Math.random();
    const contact= new ContactDto(conactId,model.name,model.phoneNumber);
    this.contacts.push(contact);  
    
    return of(contact);
  }
  update(id: number , model:UpdateContact): Observable<Contact>{
   const contact=this.getContact(id);
   if(model.name) contact.name= model.name;
   if(model.phoneNumber) contact.phoneNumber= model.phoneNumber;
    return of(contact);
  }
  toggleFavorate(id: number): void{
    const contact=this.getContact(id);
    contact.isFavorate=!contact.isFavorate;
  }
}

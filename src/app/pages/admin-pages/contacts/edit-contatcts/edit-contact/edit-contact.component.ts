import { DynamicForm } from './../../../../../shared/dynamic-form/models/dynamic-form.model';
import { Contact } from './../../models/contact.model';
import { Component, OnInit } from '@angular/core';
import { NbGlobalPhysicalPosition, NbToastRef, NbToastrService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../../../../../shared/loading-modal/loading-modal.service';
import { ContactsService } from '../../contatcts.service';
import { finalize, map, switchMap } from 'rxjs/operators';
import { ResponseModel } from '../../../../../models';
import { DynamicTextBox } from '../../../../../shared/dynamic-form/items';
import { Validators } from '@angular/forms';
import { TextBoxTypes } from '../../../../../shared/dynamic-form/components/text-box/models';
import { UpdateContact } from '../../models/update-contact.model';
import { ObjectHelper } from '../../../../../utils/object.helper';

@Component({
  selector: 'ngx-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {
  contact: Contact;

  dynamicForm: DynamicForm;
  submitButtonTextContent: string;

  toast: NbToastRef;

  constructor(
    private router: Router,
    private nbToastrService: NbToastrService,
    private activatedRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private contactService: ContactsService
  ) {}

  ngOnInit() {
    this.activatedRoute.params
      .pipe(
        switchMap((params: any) => this.contactService.getById(params.id)),
        map((res: ResponseModel<Contact>) => res.result)
      )
      .subscribe((contact: Contact) => {
        this.contact = contact;
        this.initForm();
      });
  }

  initForm(): void {
    this.submitButtonTextContent = "update_contact";

    this.dynamicForm = new DynamicForm({
      items: {
        name: new DynamicTextBox({
          label: "name",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "Enter Name",
          },
        }),
        phoneNumber: new DynamicTextBox({
          label: "phone number",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter phone number",
          },
        }),
      }
    });
  }

  onSubmit(dfValues: UpdateContact): void {
    dfValues = ObjectHelper.IgnoreUnChangedValues(dfValues, this.contact);
    this.loadingService.openModal({ message: "Updating contact..." });
    this.contactService
      .update(this.contact.id, dfValues)
      .pipe(
        finalize(() => {
          this.loadingService.closeModal();
        })
      )
      .subscribe(() => {
        this.toast = this.nbToastrService.show(
          "contact has been updated successfully",
          "Edit contact",
          {
            status: "warning",
            hasIcon: true,
            icon: "checkmark-circle-2-outline",
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );

        this.router.navigate(["/pages/contacts/"]);
      });
  }
}

import { AddBackEndUser } from "./../models/add-backend-user.model";
import { Component, OnInit } from "@angular/core";
import { Validators } from "@angular/forms";
import { Router } from "@angular/router";
import {
  NbGlobalPhysicalPosition,
  NbToastRef,
  NbToastrService,
} from "@nebular/theme";
import { finalize } from "rxjs/operators";
import { TextBoxTypes } from "../../../../shared/dynamic-form/components/text-box/models";
import {
  DynamicDropdown,
  DynamicNumberInput,
  DynamicTextBox,
} from "../../../../shared/dynamic-form/items";
import { DynamicForm } from "../../../../shared/dynamic-form/models";
import { LoadingService } from "../../../../shared/loading-modal/loading-modal.service";
import { EnumHelper } from "../../../../utils/enums.helper";
import { AddUser } from "../models/add-user.model";

import { UserService } from "../user.service";
import { UserRoles } from "../models/user-role.enum";

@Component({
  selector: "ngx-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"],
})
export class AddUserComponent implements OnInit {
  dynamicForm: DynamicForm;
  submitButtonTextContent: string;

  toast: NbToastRef;

  constructor(
    private router: Router,
    private nbToastrService: NbToastrService,
    private loadingService: LoadingService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.submitButtonTextContent = "create_user";

    this.dynamicForm = new DynamicForm({
      items: {
        firstName: new DynamicTextBox({
          label: "first_name",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_first_name",
          },
        }),
        lastName: new DynamicTextBox({
          label: "last_name",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_last_name",
          },
        }),
        phoneNumber: new DynamicTextBox({
          label: "phone",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_phone_number",
          },
        }),
        password: new DynamicTextBox({
          label: "password",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern(
              "(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&?]).*$"
            ),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_password",
          },
        }),
        email: new DynamicTextBox({
          label: "email",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.email,
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_email",
          },
        }),
        country: new DynamicTextBox({
          label: "country",
          validators: [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_country",
          },
        }),
        role: new DynamicDropdown({
          label: "role",
          validators: [Validators.required],
          context: {
            options: EnumHelper.EnumToArray(UserRoles),
            placeholder: "select_role",
          },
        }),
      },
    });
  }

  onSubmit(dfValues: AddUser): void {
    this.loadingService.openModal({ message: "Creating user..." });

    const model: AddBackEndUser = {
      firstName: dfValues.firstName,
      lastName: dfValues.lastName,
      credentials: {
        phoneNumber: dfValues.phoneNumber,
        password: dfValues.password,
      },
      email: dfValues.email,
      country: dfValues.country,
    };

    if (dfValues.role.toString() === "1") {
      this.userService
        .createEmployee(model)
        .pipe(
          finalize(() => {
            this.loadingService.closeModal();
          })
        )
        .subscribe(() => {
          this.toast = this.nbToastrService.show(
            "Employee has been created successfully",
            "Add Employee",
            {
              status: "success",
              hasIcon: true,
              icon: "checkmark-circle-2-outline",
              position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
            }
          );

          this.router.navigate(["/pages/user/"]);
        });
    }
    if (dfValues.role.toString() === "2") {
      this.userService
        .createUser(model)
        .pipe(
          finalize(() => {
            this.loadingService.closeModal();
          })
        )
        .subscribe(() => {
          this.toast = this.nbToastrService.show(
            "User has been created successfully",
            "Add User",
            {
              status: "success",
              hasIcon: true,
              icon: "checkmark-circle-2-outline",
              position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
            }
          );

          this.router.navigate(["/pages/user/"]);
        });
    }
  }
}

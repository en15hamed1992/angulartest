export interface UpdateUser{
    firstName?: string,
    lastName?: string,
    email?: string,
    country?: string,
    status?: number
}
export interface AddUser{
    firstName:string,
    lastName:string,
    phoneNumber:string,
    password:string,
    email:string,
    country:string,
    role:number;
}
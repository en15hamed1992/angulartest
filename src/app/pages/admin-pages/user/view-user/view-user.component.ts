import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { ResponseModel } from '../../../../models/response.model';
import { DateFieldComponent } from '../../../../shared/data-view/components/date-field/date-field.component';
import { DataViewFields } from '../../../../shared/data-view/models';
import { User } from '../models/User.model';
import { UserService } from '../user.service';

@Component({
  selector: 'ngx-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  user: User;

  userViewFields: DataViewFields<User>;

  renderPayments: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) {
    this.renderPayments = false;
  }

  ngOnInit() {
    this.activatedRoute.params
      .pipe(
        switchMap((params: any) => this.userService.getById(params.id)),
        map((res: ResponseModel<User>) => res.result)
      )
      .subscribe((user: User) => {
        this.user = user;
        
        this.initView();
      });
  }

  initView(): void {
    this.userViewFields = {
     
      firstName: {
        title: "first_name",
      },
     
      lastName: {
        title: "last_name",
      },
      phoneNumber: {
        title: "phone",
      },
      
      email: {
        title: "email",
      },
      country: {
        title: "country",
      },
      status: {
        title: "status",
      },
      role: {
        title: "role",
      },
     
      createDate: {
        title: "create_date",
        renderComponent: DateFieldComponent,
      },
    };
  }

}

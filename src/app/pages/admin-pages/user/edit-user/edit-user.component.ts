import { Component, OnInit } from "@angular/core";
import { Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import {
  NbGlobalPhysicalPosition,
  NbToastRef,
  NbToastrService,
} from "@nebular/theme";
import { finalize, map, switchMap } from "rxjs/operators";
import { ResponseModel } from "../../../../models/response.model";
import { TextBoxTypes } from "../../../../shared/dynamic-form/components/text-box/models";
import {
  DynamicDropdown,
  DynamicNumberInput,
  DynamicTextBox,
} from "../../../../shared/dynamic-form/items";
import { DynamicForm } from "../../../../shared/dynamic-form/models";
import { PositiveNumberValidator } from "../../../../shared/dynamic-form/validators";
import { LoadingService } from "../../../../shared/loading-modal/loading-modal.service";
import { EnumHelper } from "../../../../utils/enums.helper";
import { ObjectHelper } from "../../../../utils/object.helper";
import { UpdateUser } from "../models/update-user.model";
import { UserRoles } from "../models/user-role.enum";
import { User } from "../models/User.model";
import { UserService } from "../user.service";

@Component({
  selector: "ngx-edit-user",
  templateUrl: "./edit-user.component.html",
  styleUrls: ["./edit-user.component.scss"],
})
export class EditUserComponent implements OnInit {
  user: User;

  dynamicForm: DynamicForm;
  submitButtonTextContent: string;

  toast: NbToastRef;

  constructor(
    private router: Router,
    private nbToastrService: NbToastrService,
    private activatedRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.activatedRoute.params
      .pipe(
        switchMap((params: any) => this.userService.getById(params.id)),
        map((res: ResponseModel<User>) => res.result)
      )
      .subscribe((user: User) => {
        this.user = user;
        this.initForm();
      });
  }

  initForm(): void {
    this.submitButtonTextContent = "update_user";

    this.dynamicForm = new DynamicForm({
      items: {
        firstName: new DynamicTextBox({
          label: "first_name",
          value: this.user.firstName,
          validators: [Validators.minLength(3), Validators.maxLength(50)],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_first_name",
          },
        }),
        lastName: new DynamicTextBox({
          label: "last_name",
          value: this.user.lastName,
          validators: [Validators.minLength(3), Validators.maxLength(50)],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_last_name",
          },
        }),
        email: new DynamicTextBox({
          label: "email",
          value: this.user.email,
          validators: [
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.email,
          ],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_email",
          },
        }),
        country: new DynamicTextBox({
          label: "country",
          value: this.user.country,
          validators: [Validators.minLength(3), Validators.maxLength(50)],
          context: {
            type: TextBoxTypes.TEXT,
            placeholder: "enter_country",
          },
        }),
        role: new DynamicDropdown({
          label: "role",
          value: this.user.role,
          validators: [],
          context: {
            options: EnumHelper.EnumToArray(UserRoles),
            placeholder: this.user.role,
          },
        }),
      },
    });
  }

  onSubmit(dfValues: UpdateUser): void {
    dfValues = ObjectHelper.IgnoreUnChangedValues(dfValues, this.user);
    this.loadingService.openModal({ message: "Updating user..." });
    this.userService
      .update(`${this.user.id}`, dfValues)
      .pipe(
        finalize(() => {
          this.loadingService.closeModal();
        })
      )
      .subscribe(() => {
        this.toast = this.nbToastrService.show(
          "User has been updated successfully",
          "Edit User",
          {
            status: "warning",
            hasIcon: true,
            icon: "checkmark-circle-2-outline",
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );

        this.router.navigate(["/pages/user/"]);
      });
  }
}

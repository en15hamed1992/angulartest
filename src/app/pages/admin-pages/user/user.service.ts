import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ResponseModel } from '../../../models/response.model';
import { ID_KEY } from '../../../utils';
import { AddBackEndUser } from './models/add-backend-user.model';
import { AddUser } from './models/add-user.model';
import { UpdateUser } from './models/update-user.model';
import { User } from './models/User.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly _endpoint: string = `${environment.API_BASE_URL}/api/Users`;
  constructor(private http: HttpClient) {}

  get endpoint(): string {
    return this._endpoint;
  }
  getAll (options?: { params?: any; }):Observable<ResponseModel<User>>{
    return this.http.get<ResponseModel<User>>(`${this.endpoint}/GetAll`,options);
  }

  getById(id:string): Observable<ResponseModel<User>>{
    return this.http.get<ResponseModel<User>>(`${this.endpoint}/Get?${ID_KEY}=${id}`)
  }

  createUser(model:AddBackEndUser) : Observable<{success:boolean,message:string}>{
    return this.http.post<{success:boolean,message:string}>(`${this.endpoint}/AddUser`,model)
  }
  createEmployee(model:AddBackEndUser) : Observable<{success:boolean,message:string}>{
    return this.http.post<{success:boolean,message:string}>(`${this.endpoint}/AddEmployee`,model)
  }

  update(id: string , model:UpdateUser): Observable<ResponseModel<User>>{
    return this.http.put<ResponseModel<User>>
    (`${this._endpoint}/UpdateUser?${ID_KEY}=${id}`,
    model);
  }
  changeStatus(id: string , status:string): Observable<ResponseModel<User>>{
    return this.http.put<ResponseModel<User>>
    (`${this._endpoint}/ChangeUserStatu?${ID_KEY}=${id}`,
    status);
  }
}

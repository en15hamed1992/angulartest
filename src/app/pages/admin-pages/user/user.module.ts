import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

const routes: Routes = [
  {
    path: "",
    component: ViewUsersComponent,
  },
  {
    path: "view/:id",
    component: ViewUserComponent,
  },
  {
    path: "edit/:id",
    component: EditUserComponent,
  },
  {
    path: "add",
    component: AddUserComponent,
  },
];

@NgModule({
  declarations: [AddUserComponent, EditUserComponent, ViewUserComponent, ViewUsersComponent],
  imports:[CommonModule, RouterModule.forChild(routes), SharedModule],
})
export class UserModule { }

import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  NbGlobalPhysicalPosition,
  NbToastRef,
  NbToastrService,
} from "@nebular/theme";

import { finalize } from "rxjs/operators";
import { ConfirmationService } from "../../../../shared/confirmation-modal/confirmation.service";
import { CustomListFilterComponent } from "../../../../shared/custom-list-filter/custom-list-filter.component";
import { CustomMinMaxDateFilterComponent } from "../../../../shared/custom-min-max-date-filter/custom-min-max-date-filter.component";
import { CustomMoreLessFilterComponent } from "../../../../shared/custom-more-less-filter/custom-more-less-filter.component";
import { CustomMultiSelectFilterComponent } from "../../../../shared/custom-select-filter/custom-select-filter.component";
import { DashboardTableModifiers } from "../../../../shared/dashboard-table/dashboard-table-modifiers";
import {
  DashboardTableAction,
  DashboardTableActions,
  DashboardTableColumns,
  DashboardTableFilter,
} from "../../../../shared/dashboard-table/models";
import { DateCellComponent } from "../../../../shared/date-cell/date-cell.component";
import { LoadingService } from "../../../../shared/loading-modal/loading-modal.service";
import { AppHelpers, AppKeywords } from "../../../../utils";
import { EnumHelper } from "../../../../utils/enums.helper";
import { UserRoles } from "../models/user-role.enum";
import { UserStatus } from "../models/user-status.enum";
import { User } from "../models/User.model";
import { UserService } from "../user.service";

@Component({
  selector: "ngx-view-users",
  templateUrl: "./view-users.component.html",
  styleUrls: ["./view-users.component.scss"],
})
export class ViewUsersComponent implements OnInit {
  columns: DashboardTableColumns;
  actions: DashboardTableAction[];
  filters: DashboardTableFilter[];
  sortFieldValueMap: { [key: string]: string };
  modifiers: DashboardTableModifiers;

  toast: NbToastRef;

  loading: boolean;
  constructor(
    private router: Router,
    private toastrService: NbToastrService,
    private loadingService: LoadingService,
    private confirmationService: ConfirmationService,
    public userService: UserService
  ) {}
  ngOnDestroy(): void {
    this.confirmationService.closeModal();
    this.loadingService.closeModal();
  }
  ngOnInit(): void {
    this.initColumns();
    this.initActions();
    this.initFilters();
    this.initSortValueMap();

    this.loading = true;
  }

  initColumns(): void {
    this.columns = {
      firstName: {
        title: "first_name",
        type: "text",
      },
      lastName: {
        title: "last_name",
        type: "text",
      },
      email: {
        title: "email",
        type: "text",
      },
      phoneNumber: {
        title: "phone",
        type: "text",
      },
      country: {
        title: "country",
        type: "text",
      },
      status: {
        title: "status",
        type: "text",
        filter: {
          type: "custom",
          component: CustomMultiSelectFilterComponent,
          config: {
            options: EnumHelper.EnumToArray(UserStatus),
          },
        },
      },
      role: {
        title: "role",
        type: "text",
        filter: {
          type: "custom",
          component: CustomMultiSelectFilterComponent,
          config: {
            options: EnumHelper.EnumToArray(UserRoles),
          },
        },
      },
      createDate: {
        title: "create_date",
        type: "custom",
        renderComponent: DateCellComponent,
        filter: {
          type: "custom",
          component: CustomMinMaxDateFilterComponent,
          config: {
            min: {
              id: "min-createdDate",
              placeholder: "after_date",
            },
            max: {
              id: "max-createdDate",
              placeholder: "before_date",
            },
          },
        },
        sort: false,
      },
    };
  }

  initActions(): void {
    this.actions = [
      {
        action: DashboardTableActions.UPDATE,
      },
      {
        action: DashboardTableActions.VIEW_ONE,
      },

      {
        action: DashboardTableActions.CREATE,
      },
    ];
  }

  initFilters(): void {
    this.filters = [
      {
        columnId: "firstName",
        key: "firstName",
      },
      {
        columnId: "lastName",
        key: "lastName",
      },
      {
        columnId: "email",
        key: "email",
      },
      {
        columnId: "phoneNumber",
        key: "phoneNumber",
      },
      {
        columnId: "status",
        key: "status",
      },
      {
        columnId: "role",
        key: "Role",
      },

      {
        columnId: "min-createdDate",
        key: "StartDate",
      },
      {
        columnId: "max-createdDate",
        key: "EndDate",
      },
    ];
  }

  initSortValueMap(): void {
    this.sortFieldValueMap = {
      name: "0",
      requiredScore: "1",
    };
  }

  onCreate(modifiers: DashboardTableModifiers): void {
    this.modifiers = modifiers;
  }

  onAdd(): void {
    this.router.navigate(["/pages/user/add"]);
  }
  onView(user: User): void {
    this.router.navigate(["/pages/user/view", user.id]);
  }

  onEdit(user: User): void {
    this.router.navigate(["/pages/user/edit", user.id]);
  }

  onDelete(user: User) {
    const confirmkeyword = AppKeywords.ACTIVATE_KEYWORD.toLowerCase();
    const loadKeyword = AppKeywords.ACTIVATING_KEYWORD.toLowerCase();

    this.confirmationService.openModal({
      title: `${AppHelpers.uppercaseFirst(confirmkeyword)} User`,
      message: `Are you sure you want to ${confirmkeyword} User "${user.firstName}"  ${user.lastName}?`,
      onConfirm: () => {
        this.loadingService.openModal({
          message: `${loadKeyword} User "${user.firstName}"...`,
        });
      },
    });
  }

  onLoad(): void {
    this.loading = false;
  }

  resetFilters(): void {
    this.modifiers.resetFilters();
  }
}

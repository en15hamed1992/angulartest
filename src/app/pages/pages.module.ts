import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NbAlertModule, NbMenuModule } from "@nebular/theme";

import { ThemeModule } from "../@theme/theme.module";
import { PagesComponent } from "./pages.component";
import { PagesRoutingModule } from "./pages-routing.module";
import { MainPageComponent } from "./admin-pages/main-page/main-page.component";
import { NotFoundPageComponent } from "./admin-pages/not-found-page/not-found-page.component";

@NgModule({
    imports: [CommonModule, NbAlertModule, PagesRoutingModule, ThemeModule, NbMenuModule],
    declarations: [PagesComponent, MainPageComponent, NotFoundPageComponent],
})
export class PagesModule {}

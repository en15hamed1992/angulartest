import { RoleProviderService } from "./../services/role-provider.service";
import { Injectable } from "@angular/core";
import { NbMenuItem } from "@nebular/theme";

@Injectable({
    providedIn: "root",
})
export class PagesMenuService {
    constructor(private roleProvider: RoleProviderService) {}

    public getPages(menuItems: NbMenuItem[]) {
        menuItems = JSON.parse(JSON.stringify(menuItems));

        const pages: any[] = [];

        menuItems.forEach((item: NbMenuItem) => {
            if (
                !item.data?.permissions ||
                this.roleProvider.isGranted(item.data?.permissions, item.data?.operator || "and")
            ) {
                if (item.children) {
                    item.children = this.getPages(item.children);
                }

                pages.push(item);
            }
        });

        return pages;
    }
}

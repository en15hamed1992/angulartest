import { PagesMenuService } from "./pages-menu.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NbLayoutDirectionService, NbMenuBag, NbMenuService } from "@nebular/theme";
import { BehaviorSubject } from "rxjs";

import { MENU_ITEMS } from "./pages-menu";
import { TranslateService } from "../translation/translate.service";
import { AuthService } from "../custom-auth/services/auth.service";

@Component({
    selector: "ngx-pages",
    styleUrls: ["pages.component.scss"],
    templateUrl: "./pages.component.html",
})
export class PagesComponent implements OnInit {
    menu: BehaviorSubject<any>;

    constructor(
        private router: Router,
        private directionService: NbLayoutDirectionService,
        private translateService: TranslateService,
        private pagesMenuService: PagesMenuService,
        private menuService: NbMenuService,
        private authService: AuthService
    ) {
        this.menu = new BehaviorSubject(pagesMenuService.getPages(MENU_ITEMS));
    }

    ngOnInit() {
        this.menuService.onItemClick().subscribe((bag: NbMenuBag) => {
            if (bag.item.data?.meta === "LOGOUT") {
                this.authService.logout().subscribe(() => {
                    this.router.navigate(["/auth/login"]);
                });
            }
        });

        this.directionService.onDirectionChange().subscribe(() => this.updateMenu());
    }

    updateMenu() {
        const newMenu = this.nestedTranslator(this.pagesMenuService.getPages(MENU_ITEMS));

        this.menu.next(newMenu);
    }

    nestedTranslator(menu: any) {
        return menu.map((item: any) => {
            if (item.children && item.children.length)
                return {
                    ...item,
                    title: this.translateService.translate(item.title),
                    children: this.nestedTranslator(item.children),
                };
            else
                return {
                    ...item,
                    title: this.translateService.translate(item.title),
                };
        });
    }
}

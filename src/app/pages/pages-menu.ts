import { NbMenuItem } from "@nebular/theme";

import { AppPermissions } from "../utils";

/**
 * you can change the icon from nebular-icons
 *
 * https://github.com/akveo/nebular-icons/tree/master/src/icons
 */
export const MENU_ITEMS: NbMenuItem[] = [
  // like game header 
  {
    title: "Template",
    group: true,
  },
  // users
  {

    title: "contacts",
    icon: "person-outline",
    link: "/pages/contacts",
   
  },
  {
    title: "auth",
    icon: "lock-outline",
    children: [
      {
        title: "switch_account",
        link: "/auth/login",
        icon: "toggle-right-outline",
      },
      {
        title: "logout",
        data: {
          meta: "LOGOUT",
        },
        icon: "log-out-outline",
      },
    ],
  },
];

import { ResponseModel } from './../models/response.model';
import {
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { tokenCredentials } from "../guards/credentials.model";

import { ErrorService } from "../shared/error-modal/error-modal.service";
import { Router } from '@angular/router';

@Injectable({
  providedIn: "root",
})
export class HttpErrorInterceptorService implements HttpInterceptor {
  constructor(private errorService: ErrorService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   
    
    // Temp
    // --------------------------------------------------------------------------------
    // const url: string[] = request.url.split("/");

    // if (url[url.length - 2] === "check" && url[url.length - 3] === "voucher-code") {
    //   return next.handle(request);
    // }
    // --------------------------------------------------------------------------------
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        // console.log(err);
        this.errorService.openModal({
          error: err.statusText,
          message: err.error.message || "Something went wrong, Please try again later",
        });

        return throwError(err);
      })
    );
  }
}

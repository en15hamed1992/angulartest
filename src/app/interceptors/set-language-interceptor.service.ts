import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpHeaders,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { NbLayoutDirection, NbLayoutDirectionService } from "@nebular/theme";

@Injectable({
  providedIn: "root",
})
export class SetLanguageInterceptorService implements HttpInterceptor {
  constructor(private directionService: NbLayoutDirectionService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const lang =
      this.directionService.getDirection() == NbLayoutDirection.RTL
        ? "ar"
        : "en";

    return next.handle(
      req.clone({
        setHeaders: {
          "Accept-Language": req.headers.get("Accept-Language") || lang,
        },
      })
    );
  }
}

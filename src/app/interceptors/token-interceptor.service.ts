import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpErrorResponse,
} from "@angular/common/http";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { environment } from "../../environments/environment";
import { AuthService } from "../custom-auth/services/auth.service";
import { catchError, filter, switchMap, take } from "rxjs/operators";
import { Router } from "@angular/router";

const SKIP_LIST = [
  `${environment.API_BASE_URL}${environment.login_URL}`,
  `${environment.API_BASE_URL}${environment.refresh_Token_URL}`,
];
@Injectable({
  providedIn: "root",
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private router: Router, private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // console.log("TokenInterceptorService -> intercept", { request });
    if (SKIP_LIST.includes(request.url)) {
      return next.handle(request);
    }

    const authAppToken = this.authService.getAppToken();
    if (authAppToken && authAppToken.token) {
      request = this.addToken(request, authAppToken.token);
    }

    return next.handle(request).pipe(
      catchError((error) => {
        // console.error(error);
        if (error instanceof HttpErrorResponse && error.status == 403) {
          return this.handleRefreshToken(request, next);
        } else {
          return throwError(error);
        }
      })
    );
  }

  addToken(request: HttpRequest<any>, token: string) {
    return request.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
  }

  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  private handleRefreshToken(request: HttpRequest<any>, next: HttpHandler) {
    // console.log("handleRefreshToken -->", {
    //   url: request.url,
    //   isRefreshing: this.isRefreshing,
    // });

    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      return this.authService.refreshToken().pipe(
        switchMap((response: any) => {
          // console.log({ refreshTokenRequestResponse: response });
          this.isRefreshing = false;
          this.refreshTokenSubject.next(response.result.token);
          return next
            .handle(this.addToken(request, response.result.token))
            .pipe(
              catchError((err) => {
                this.router.navigate(["/auth/login"]);
                return throwError(err);
              })
            );
        })
      );
    } else {
      return this.refreshTokenSubject.pipe(
        filter((authTokenInfo) => authTokenInfo != null),
        take(1),
        switchMap((authTokenInfo) => {
          return next.handle(this.addToken(request, authTokenInfo)).pipe(
            catchError((err) => {
              this.router.navigate(["/auth/login"]);
              return throwError(err);
            })
          );
        })
      );
    }
  }
}

// const jwtToken =
// "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9tb2JpbGVwaG9uZSI6IjEyMzEyMzEyMyIsIklkIjoiMSIsInJvbGUiOiJBZG1pbiIsIlN0YXR1cyI6ImFjdGl2YXRlZCIsImp0aSI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6MTYxODkxMzU0NCwiZXhwIjoxNjE4OTk5OTQ0LCJpYXQiOjE2MTg5MTM1NDR9.6vGv8tJWLXr8qI6MSoTbT7wnFD8tHyZUlZTI0a_sHiEGhg-eVnVnATmZlSN4UkrESZyimSI4XDMEBFNm90Jbtw";

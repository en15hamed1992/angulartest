import { Injectable } from "@angular/core";
import { NbLayoutDirectionService } from "@nebular/theme";

import { AppLanguages } from "./app-languages";
import * as arabicTranslation from "./translation-ar.json";
import * as englishTranslation from "./translation-en.json";

@Injectable({
    providedIn: "root",
})
export class TranslateService {
    constructor(private directionService: NbLayoutDirectionService) {}

    translate(key: string) {
        const currentLanguage =
            this.directionService.getDirection() == "ltr"
                ? AppLanguages.ENGLISH
                : AppLanguages.ARABIC;

        if (currentLanguage == AppLanguages.ARABIC) {
            return (arabicTranslation as any).default[key] || key;
        }

        return (englishTranslation as any).default[key] || key;
    }
}

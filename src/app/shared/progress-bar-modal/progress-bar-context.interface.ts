import { Observable } from "rxjs";

export interface ProgressBarContext {
    message: string;
    value: number | Observable<number>;
}

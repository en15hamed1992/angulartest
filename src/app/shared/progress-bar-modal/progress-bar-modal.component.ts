import { Component, Input, OnInit } from "@angular/core";
import { isObservable, Observable } from "rxjs";

import { ProgressBarContext } from "./progress-bar-context.interface";

@Component({
    selector: "ngx-progress-bar-modal",
    templateUrl: "./progress-bar-modal.component.html",
    styleUrls: ["./progress-bar-modal.component.scss"],
})
export class ProgressBarModalComponent implements OnInit, ProgressBarContext {
    @Input() message: string;
    @Input() value: number | Observable<number>;

    constructor() {}

    ngOnInit(): void {}

    isObservabel(obsToCheck: any): boolean {
        return isObservable(obsToCheck);
    }

    isFunction(functionToCheck: any) {
        return functionToCheck && {}.toString.call(functionToCheck) === "[object Function]";
    }
}

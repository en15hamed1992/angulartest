import { Injectable } from "@angular/core";
import { NbDialogRef, NbDialogService } from "@nebular/theme";

import { ProgressBarContext } from "./progress-bar-context.interface";
import { ProgressBarModalComponent } from "./progress-bar-modal.component";

@Injectable({
    providedIn: "root",
})
export class ProgressBarService {
    private _dialog: NbDialogRef<ProgressBarModalComponent>;
    private _isOpen = false;

    constructor(private nbDialogService: NbDialogService) {}

    openModal(context?: Partial<ProgressBarContext>): void {
        this._dialog = this.nbDialogService.open(ProgressBarModalComponent, {
            hasBackdrop: true,
            closeOnBackdropClick: false,
            context: {
                message: context?.message || "",
                value: context?.value || 0,
            },
        });

        this._isOpen = true;
    }

    closeModal(): void {
        this._dialog?.close();
    }

    isModalOpen(): boolean {
        return this._isOpen;
    }
}

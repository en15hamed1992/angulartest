export interface ErrorContext {
    error: string;
    message: string;
}

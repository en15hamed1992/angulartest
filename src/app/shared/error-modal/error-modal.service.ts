import { Injectable } from "@angular/core";
import { NbDialogRef, NbDialogService } from "@nebular/theme";

import { ErrorContext } from "./error-context.model";
import { ErrorModalComponent } from "./error-modal.component";

@Injectable({
    providedIn: "root",
})
export class ErrorService {
    private _dialog: NbDialogRef<ErrorModalComponent>;
    private _isOpen = false;

    constructor(private dialogService: NbDialogService) {}

    openModal(context: ErrorContext): void {
        this._dialog = this.dialogService.open(ErrorModalComponent, {
            context: context,
        });

        this._isOpen = true;
    }

    closeModal(): void {
        this._dialog?.close();

        this._isOpen = false;
    }

    isModalOpen(): boolean {
        return this._isOpen;
    }
}

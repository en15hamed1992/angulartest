import { Component, Input, OnInit } from "@angular/core";
import { ErrorService } from "./error-modal.service";

@Component({
    selector: "ngx-error-modal",
    templateUrl: "./error-modal.component.html",
    styleUrls: ["./error-modal.component.scss"],
})
export class ErrorModalComponent implements OnInit {
    @Input("error") error: string;
    @Input("message") message: string;

    constructor(private errorService: ErrorService) {}

    ngOnInit(): void {}

    dismiss(): void {
        this.errorService.closeModal();
    }
}

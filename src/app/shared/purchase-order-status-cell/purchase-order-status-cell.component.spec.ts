import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PurchaseOrderStatusCellComponent } from "./purchase-order-status-cell.component";

describe("PurchaseOrderStatusCellComponent", () => {
    let component: PurchaseOrderStatusCellComponent;
    let fixture: ComponentFixture<PurchaseOrderStatusCellComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PurchaseOrderStatusCellComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PurchaseOrderStatusCellComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

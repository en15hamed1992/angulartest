import { Router } from "@angular/router";
import { Component, OnInit, Input } from "@angular/core";

@Component({
    selector: "ngx-purchase-order-status-cell",
    templateUrl: "./purchase-order-status-cell.component.html",
    styleUrls: ["./purchase-order-status-cell.component.scss"],
})
export class PurchaseOrderStatusCellComponent implements OnInit {
    @Input() value: string;
    @Input() rowData: any;

    constructor(private router: Router) {}

    ngOnInit(): void {}

    onAddSerials() {
        this.router.navigateByUrl(`/pages/purchase-orders/view/${this.rowData.id}`);
    }
}

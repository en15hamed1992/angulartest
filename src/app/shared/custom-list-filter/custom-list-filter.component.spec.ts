import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CustomListFilterComponent } from "./custom-list-filter.component";

describe("CustomListFilterComponent", () => {
    let component: CustomListFilterComponent;
    let fixture: ComponentFixture<CustomListFilterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CustomListFilterComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustomListFilterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

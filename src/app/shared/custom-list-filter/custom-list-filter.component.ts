import { Component, OnInit, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { distinctUntilChanged, debounceTime } from "rxjs/operators";
import { DefaultFilter } from "ng2-smart-table";
import { NbLayoutDirectionService } from "@nebular/theme";
import { TranslateService } from "../../translation/translate.service";

@Component({
    selector: "ngx-custom-list-filter",
    templateUrl: "./custom-list-filter.component.html",
    styleUrls: ["./custom-list-filter.component.scss"],
})
export class CustomListFilterComponent extends DefaultFilter implements OnInit {
    listControl = new FormControl();

    options: {
        title: string;
        value: string;
    }[];

    constructor(
        private directionService: NbLayoutDirectionService,
        private translateService: TranslateService
    ) {
        super();
    }

    ngOnInit() {
        this.directionService.onDirectionChange().subscribe(() => {
            this.options = this.column.filter.config.options.map(
                (option: { title: string; value: string }) => {
                    return {
                        title: this.translateService.translate(option.title),
                        value: option.value,
                    };
                }
            );
        });

        this.listControl.valueChanges
            .pipe(debounceTime(400), distinctUntilChanged())
            .subscribe((value: any) => {
                value = String(value);

                this.query = value !== null ? value : "";
                this.setFilter();
            });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.query) {
            this.query = changes.query.currentValue;
        }
    }
}

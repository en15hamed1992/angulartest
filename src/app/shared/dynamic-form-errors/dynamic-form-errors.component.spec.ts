import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { DynamicFormErrorsComponent } from "./dynamic-form-errors.component";

describe("DynamicFormErrorsComponent", () => {
    let component: DynamicFormErrorsComponent;
    let fixture: ComponentFixture<DynamicFormErrorsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DynamicFormErrorsComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DynamicFormErrorsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

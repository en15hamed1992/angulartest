import { Component, OnInit, Input } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { fadeInTrigger } from "../animation";

@Component({
    selector: "dynamic-form-errors",
    templateUrl: "./dynamic-form-errors.component.html",
    styleUrls: ["./dynamic-form-errors.component.scss"],
    animations: [fadeInTrigger],
})
export class DynamicFormErrorsComponent implements OnInit {
    @Input("formInterface") formInterface: AbstractControl;
    @Input("formkey") formkey: string;

    constructor() {}

    ngOnInit(): void {}

    keys(obj: any) {
        return Object.keys(obj);
    }
}

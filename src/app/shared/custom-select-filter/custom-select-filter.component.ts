import { Component, OnInit, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { DefaultFilter } from "ng2-smart-table";
import { TranslateService } from "../../translation/translate.service";
import { NbLayoutDirectionService } from "@nebular/theme";

@Component({
  selector: "ngx-custom-select-filter",
  templateUrl: "./custom-select-filter.component.html",
  styleUrls: ["./custom-select-filter.component.scss"],
})
export class CustomMultiSelectFilterComponent
  extends DefaultFilter
  implements OnInit {
  selectedItemsControl = new FormControl();

  options: {
    title: string;
    value: string;
  }[];

  constructor(
    private directionService: NbLayoutDirectionService,
    private translateService: TranslateService
  ) {
    super();
  }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(() => {
      this.options = [
        ...this.column.filter.config.options.map(
          (option: { title: string; value: string }) => {
            return {
              title: this.translateService.translate(option.title),
              value: option.value,
            };
          }
        ),
      ];
    });

    this.selectedItemsControl.valueChanges
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe((value: any) => {
        this.query = value ?? "";
        
        this.setFilter();
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.query) {
      
      this.query = changes.query.currentValue;
    }
  }
}

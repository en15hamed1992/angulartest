import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CustomMultiSelectFilterComponent } from "./custom-select-filter.component";

describe("CustomMultiSelectFilterComponent", () => {
    let component: CustomMultiSelectFilterComponent;
    let fixture: ComponentFixture<CustomMultiSelectFilterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CustomMultiSelectFilterComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustomMultiSelectFilterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

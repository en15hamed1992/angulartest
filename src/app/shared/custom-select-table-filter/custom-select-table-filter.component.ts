import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { NbDialogRef, NbDialogService } from "@nebular/theme";
import { DefaultFilter } from "ng2-smart-table";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

import { Status } from "../../@theme/constants/status";
import { SelectComponent, SelectTableContext } from "./models";

@Component({
  selector: "ngx-custom-select-table-filter",
  templateUrl: "./custom-select-table-filter.component.html",
  styleUrls: ["./custom-select-table-filter.component.scss"],
})
export class CustomSelectTableFilterComponent
  extends DefaultFilter
  implements OnInit {
  control: FormControl = new FormControl();

  selectedItems: any | any[];
  modalBtnText: string;
  dialog: NbDialogRef<SelectComponent>;
  spinnerConfig = {
    bdColor: "transparent",
    color: Status.PRIMARY,
    type: "ball-clip-rotate",
    size: "default",
    fullScreen: false,
  };

  constructor(private dialogService: NbDialogService) {
    super();
  }

  ngOnInit(): void {
    this.setModalBtnText();

    this.control.valueChanges
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe((value: any) => {
        const filterValue = this.isMultiSelect
          ? value.map((doc) => doc[this.column.filter.config.filterKey])
          : value[this.column.filter.config.filterKey];

        console.log({ filterValue,value,config:this.column.filter.config });
        
        this.query = filterValue ?? "";
        this.setFilter();
      });
  }

  open() {
    this.dialog = this.dialogService.open(
      this.column.filter.config.renderComponent,
      {
        context: {
          modal: true,
          params: this.column.filter.config.params || {},
          multiSelect: this.column.filter.config.multiSelect,
          preSelected: this.selectedItems,
          selectFn: (selectedItems: any | any[]) => {
            this.selectedItems = selectedItems;
            this.setModalBtnText();
          },
          closeFn: () => {
            this.dialog.close();
          },
        },
        hasScroll: true,
        dialogClass: "table-modal",
      }
    );

    this.dialog.onClose.subscribe(() => {
      this.control.markAsTouched();

      this.control.setValue(this.selectedItems);
    });
  }

  setModalBtnText(): void {
    if (!this.isMultiSelect) {
      this.modalBtnText = this.selectedItems
        ? this.selectedItems[this.column.filter.config.viewKey]
        : "Select one item";
    } else {
      this.modalBtnText = this.selectedItems?.length
        ? `${this.selectedItems?.length} items selected`
        : "0 items selected";
    }
  }

  hasValue(): boolean {
    if (
      (!this.isMultiSelect && !this.selectedItems) ||
      (this.isMultiSelect && !this.selectedItems?.length)
    ) {
      return false;
    }
    return true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.query) {
      this.query = changes.query.currentValue;
    }
  }

  private get isMultiSelect() {
    return this.column.filter.config.multiSelect;
  }
}

export interface SelectComponent<T = any> {
    modal: boolean;
    params: any;
    multiSelect: boolean;
    preSelected: T | T[];
    selectFn: (selectedItems: T | T[]) => void;
    closeFn: () => void;
}

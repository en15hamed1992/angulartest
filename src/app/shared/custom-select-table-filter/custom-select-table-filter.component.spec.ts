import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CustomSelectTableFilterComponent } from "./custom-select-table-filter.component";

describe("SelectTableComponent", () => {
    let component: CustomSelectTableFilterComponent;
    let fixture: ComponentFixture<CustomSelectTableFilterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CustomSelectTableFilterComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustomSelectTableFilterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

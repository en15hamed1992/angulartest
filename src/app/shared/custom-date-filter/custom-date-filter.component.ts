import { Component, OnInit, OnChanges, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { DefaultFilter } from "ng2-smart-table";
import { distinctUntilChanged, debounceTime } from "rxjs/operators";

import { AppHelpers } from "../../utils";

@Component({
    selector: "ngx-custom-date-filter",
    templateUrl: "./custom-date-filter.component.html",
    styleUrls: ["./custom-date-filter.component.scss"],
})
export class CustomDateFilterComponent extends DefaultFilter implements OnInit, OnChanges {
    inputControl = new FormControl();

    constructor() {
        super();
    }

    ngOnInit() {
        this.inputControl.valueChanges
            .pipe(debounceTime(400), distinctUntilChanged())
            .subscribe((value: number) => {
                this.query =
                    value !== null ? AppHelpers.dateToUtc(this.inputControl.value).toString() : "";
                this.setFilter();
            });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.query) {
            this.query = changes.query.currentValue;
        }
    }
}

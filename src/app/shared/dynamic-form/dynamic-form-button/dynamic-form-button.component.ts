import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
} from "@angular/core";
import { NbThemeService } from "@nebular/theme";
import { BehaviorSubject, combineLatest, Observable } from "rxjs";
import { distinctUntilChanged, map, tap } from "rxjs/operators";
import { DynamicForm, SubmitButtonAssociations } from "../models";

@Component({
    selector: "app-dynamic-form-button",
    templateUrl: "./dynamic-form-button.component.html",
    styleUrls: ["./dynamic-form-button.component.scss"],
})
export class DynamicFormButtonComponent implements OnInit, OnChanges {
    @Input() associatedForms: SubmitButtonAssociations;
    @Input() textContent: string;
    @Input() disabled: boolean;
    @Input() transformFn: (values: any) => any;

    @Output() dfSubmit: EventEmitter<any>;

    themeName: string;
    enabled$: Observable<boolean>;

    private _associatedForms$: BehaviorSubject<SubmitButtonAssociations>;
    private _enabled$: BehaviorSubject<boolean>;

    constructor(private themeService: NbThemeService) {
        this.dfSubmit = new EventEmitter<any>();
        this._enabled$ = new BehaviorSubject<boolean>(false);
        this._associatedForms$ = new BehaviorSubject<SubmitButtonAssociations>(null);
    }

    ngOnInit(): void {
        this._associatedForms$
            .pipe(
                distinctUntilChanged(
                    (prev: SubmitButtonAssociations, curr: SubmitButtonAssociations) => {
                        return this.compare(prev, curr);
                    }
                )
            )
            .subscribe((associatedForm: SubmitButtonAssociations) => {
                this.associatedForms = associatedForm;

                this.initEnableButtonSubscription();
            });

        this.themeService.onThemeChange().subscribe((theme: any) => {
            this.themeName = theme.name;
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        const disabled = changes.disabled?.currentValue;
        const associatedForms = changes.associatedForms?.currentValue;

        this._enabled$.next(!!!disabled);
        this._associatedForms$.next(associatedForms);
    }

    initEnableButtonSubscription(): void {
        const formValidations = this.associatedForms.length
            ? (this.associatedForms as DynamicForm[]).map((form: DynamicForm) => {
                  return form.statusChange.pipe(map((status) => status.valid));
              })
            : Object.values(this.associatedForms).map((form: DynamicForm) => {
                  return form.statusChange.pipe(map((status) => status.valid));
              });

        this.enabled$ = combineLatest([...formValidations, this._enabled$]).pipe(
            map((values: boolean[]) => values.every((value: boolean) => value === true)),
            distinctUntilChanged()
        );
    }

    compare(prev: SubmitButtonAssociations, curr: SubmitButtonAssociations): boolean {
        if (curr.length) {
            return prev.length === curr.length;
        }

        if (Object.keys(prev) !== Object.keys(curr)) {
            return false;
        }

        return !Object.keys(prev).every((key) => Object.keys(curr).includes(key));
    }

    onSubmit(): void {
        let values: any;

        if (this.associatedForms.length) {
            values = [];
            (this.associatedForms as DynamicForm[]).forEach((form) => {
                values.push(form.value);
            });
        } else {
            values = {};
            Object.entries(this.associatedForms as { [key: string]: DynamicForm }).forEach(
                ([key, form]) => {
                    values[key] = form.value;
                }
            );
        }

        if (this.transformFn !== undefined && this.transformFn !== null) {
            values = this.transformFn(values);
        }

        this.dfSubmit.emit(values);
    }
}

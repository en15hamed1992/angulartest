import {
    AfterViewInit,
    Component,
    ComponentFactoryResolver,
    ElementRef,
    Input,
    OnInit,
    ViewChild,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NbDialogRef } from "@nebular/theme";

import { ItemHostDirective } from "../directives/item-host.directive";
import { DynamicForm, DynamicFormItemComponent } from "../models";

@Component({
    selector: "ngx-dynamic-form-modal",
    templateUrl: "./dynamic-form-modal.component.html",
    styleUrls: ["./dynamic-form-modal.component.scss"],
})
export class DynamicFormModalComponent implements OnInit, AfterViewInit {
    @ViewChild(ItemHostDirective, { static: true })
    ngxItemHost: ItemHostDirective;
    @ViewChild("cancelBtn", { static: false }) cancelBtn: {
        hostElement: ElementRef<HTMLButtonElement>;
    };
    @ViewChild("submitBtn", { static: false }) submitBtn: {
        hostElement: ElementRef<HTMLButtonElement>;
    };

    @Input() formObj: DynamicForm;
    @Input() title: string;
    @Input() submitTxt: string;
    @Input() validators: Validators;
    @Input() onSubmit: (result: any) => any;

    form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private componentFactoryResolver: ComponentFactoryResolver,
        private dialogRef: NbDialogRef<DynamicFormModalComponent>
    ) {}

    ngOnInit(): void {
        this.initForm();
    }

    ngAfterViewInit(): void {
        this.cancelBtn.hostElement.nativeElement.blur();
        this.submitBtn.hostElement.nativeElement.blur();
    }

    initForm(): void {
        this.form = this.formBuilder.group(
            {},
            { validators: this.formObj.validators, asyncValidators: this.formObj.asyncValidators }
        );
        this.form.valueChanges.subscribe(() => {
            this.formObj.setStatus({
                valid: this.form.valid,
                dirty: this.form.dirty,
                touched: this.form.touched,
            });
        });

        const viewContainerRef = this.ngxItemHost.viewContainerRef;
        viewContainerRef.clear();

        Object.keys(this.formObj.items).forEach((key: string) => {
            this.form.addControl(key, this.formObj.items[key].control);
        });

        Object.keys(this.formObj.items).forEach((key: string) => {
            const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
                this.formObj.items[key].renderComponent
            );
            const componentRef = viewContainerRef.createComponent<DynamicFormItemComponent>(
                componentFactory
            );

            componentRef.instance.key = key;
            componentRef.instance.itemObj = this.formObj.items[key];
            componentRef.instance.parentControl = this.form;
        });
    }

    confirm() {
        this.onSubmit(this.form.getRawValue());
        this.dismiss();
    }

    dismiss() {
        this.dialogRef.close();
    }
}

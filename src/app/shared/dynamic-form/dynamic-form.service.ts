import { Injectable } from "@angular/core";
import { NbDialogRef, NbDialogService } from "@nebular/theme";
import { DynamicFormModalComponent } from "./dynamic-form-modal/dynamic-form-modal.component";
import { DynamicFormItemContext } from "./models";

@Injectable({
    providedIn: "root",
})
export class DynamicFormService {
    private _dialog: NbDialogRef<DynamicFormModalComponent>;
    private _isOpen = false;

    constructor(private dialogService: NbDialogService) {}

    openModal(context: DynamicFormItemContext): void {
        this._dialog = this.dialogService.open(DynamicFormModalComponent, {
            hasBackdrop: true,
            closeOnBackdropClick: false,
            context: context,
            hasScroll: true,
        });

        this._isOpen = true;
    }

    closeModal(): void {
        this._dialog?.close();

        this._isOpen = false;
    }

    isModalOpen(): boolean {
        return this._isOpen;
    }
}

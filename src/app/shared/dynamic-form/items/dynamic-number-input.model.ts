import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { NumberInputContext } from "../components/number-input/models";
import { DynamicFormItem } from "../models";
import { NumberInputComponent } from "../components/number-input/number-input.component";

export interface DynamicNumberInputState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: NumberInputContext;
}

export class DynamicNumberInput extends DynamicFormItem<NumberInputContext> {
    constructor(state: DynamicNumberInputState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value !== undefined || state.value !== null ? state.value : null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: {
                ...state.context,
                step: state.context.step || 1,
            },
            renderComponent: NumberInputComponent,
            control: new FormControl(),
        });
    }
}

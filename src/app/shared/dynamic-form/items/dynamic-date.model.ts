import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { DateComponent } from "../components/date/date.component";
import { DateContext } from "../components/date/models";
import { DynamicFormItem } from "../models";

export interface DynamicDateState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: DateContext;
}

export class DynamicDate extends DynamicFormItem<DateContext> {
    constructor(state: DynamicDateState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            renderComponent: DateComponent,
            control: new FormControl(),
            context: {
                ...state.context,
                placeholder: state.context.placeholder || new Date(),
            },
        });
    }
}

import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { PhoneContext } from "../components/phone/models";
import { PhoneComponent } from "../components/phone/phone.component";
import { DynamicFormItem, DynamicFormItemTypes } from "../models";

export interface DynamicPhoneState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: PhoneContext;
}

export class DynamicPhone extends DynamicFormItem<PhoneContext> {
    constructor(state: DynamicPhoneState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: PhoneComponent,
            control: new FormControl(),
        });
    }
}

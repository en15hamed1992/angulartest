import { AsyncValidatorFn, ValidatorFn } from "@angular/forms";

export interface DynamicBaseState<TContext = any> {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: TContext;
}

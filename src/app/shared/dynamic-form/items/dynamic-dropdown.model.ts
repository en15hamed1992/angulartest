import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { DropdownComponent } from "../components/dropdown/dropdown.component";
import { DropdownContext } from "../components/dropdown/models";
import { DynamicFormItem, DynamicFormItemTypes } from "../models";

export interface DynamicDropdownState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: DropdownContext;
}

export class DynamicDropdown extends DynamicFormItem<DropdownContext> {
    constructor(state: DynamicDropdownState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: DropdownComponent,
            control: new FormControl(),
        });
    }
}

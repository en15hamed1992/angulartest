import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { CheckboxComponent } from "../components/checkbox/checkbox.component";
import { CheckboxContext } from "../components/checkbox/models";
import { DynamicFormItem, DynamicFormItemTypes } from "../models";

export interface DynamicCheckboxState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: CheckboxContext;
}

export class DynamicCheckbox extends DynamicFormItem<CheckboxContext> {
    constructor(state: DynamicCheckboxState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: CheckboxComponent,
            control: new FormControl(),
        });
    }
}

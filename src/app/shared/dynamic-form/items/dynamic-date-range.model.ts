import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { DateRangeComponent } from "../components/date-range/date-range.component";
import { DateRangeContext } from "../components/date-range/models";
import { DynamicFormItem, DynamicFormItemTypes } from "../models";

export interface DynamicDateRangeState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: DateRangeContext;
}

export class DynamicDateRange extends DynamicFormItem<DateRangeContext> {
    constructor(state: DynamicDateRangeState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: DateRangeComponent,
            control: new FormControl(),
        });
    }
}

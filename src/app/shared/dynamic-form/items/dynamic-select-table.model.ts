import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { SelectTableContext } from "../components/select-table/models";
import { SelectTableComponent } from "../components/select-table/select-table.component";
import { DynamicFormItem } from "../models";

export interface DynamicSelectTableState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: SelectTableContext;
}

export class DynamicSelectTable extends DynamicFormItem<SelectTableContext> {
    constructor(state: DynamicSelectTableState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: SelectTableComponent,
            control: new FormControl(),
        });
    }
}

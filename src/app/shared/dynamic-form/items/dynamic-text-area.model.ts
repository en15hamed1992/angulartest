import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { TextAreaContext } from "../components/text-area/models";
import { TextAreaComponent } from "../components/text-area/text-area.component";
import { DynamicFormItem, DynamicFormItemTypes } from "../models";

export interface DynamicTextAreaState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: TextAreaContext;
}

export class DynamicTextArea extends DynamicFormItem<TextAreaContext> {
    constructor(state: DynamicTextAreaState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: TextAreaComponent,
            control: new FormControl(),
        });
    }
}

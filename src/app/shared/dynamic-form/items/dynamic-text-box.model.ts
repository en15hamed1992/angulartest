import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { TextBoxContext } from "../components/text-box/models";
import { TextBoxComponent } from "../components/text-box/text-box.component";
import { DynamicFormItem } from "../models";

export interface DynamicTextBoxState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: TextBoxContext;
}

export class DynamicTextBox extends DynamicFormItem<TextBoxContext> {
    constructor(state: DynamicTextBoxState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: TextBoxComponent,
            control: new FormControl(),
        });
    }
}

import { AsyncValidatorFn, FormControl, ValidatorFn } from "@angular/forms";

import { CsvUploaderComponent } from "../components/csv-uploader/csv-uploader.component";
import { CsvUploaderContext } from "../components/csv-uploader/models/csv-uploader-context.model";
import { DynamicFormItem } from "../models";

export interface DynamicCsvUploaderState {
    label: string;
    disabled?: boolean;
    value?: any;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    context: CsvUploaderContext;
}

export class DynamicCsvUploader extends DynamicFormItem<CsvUploaderContext> {
    constructor(state: DynamicCsvUploaderState) {
        super({
            label: state.label,
            disabled: state.disabled || false,
            value: state.value || null,
            validators: state.validators || [],
            asyncValidators: state.asyncValidators || [],
            context: state.context,
            renderComponent: CsvUploaderComponent,
            control: new FormControl(),
        });
    }
}

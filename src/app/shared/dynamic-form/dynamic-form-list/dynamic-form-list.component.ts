import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from "@angular/core";
import { NbThemeService } from "@nebular/theme";
import { Subscription } from "rxjs";
import { distinctUntilChanged, filter, startWith } from "rxjs/operators";

import { ConfirmationService } from "../../confirmation-modal/confirmation.service";
import {
    DynamicForm,
    DynamicFormListInnerItemValue,
    DynamicFromListValue,
    DynamicFormDescriptor,
    DynamicFormItem,
    DynamicFormItemDescriptor,
    DynamicFormItemTypeDependent,
    DynamicFormItemTypeIndependent,
    DynamicyFormDependent,
    DynamicyFormRealtion,
} from "../models";

@Component({
    selector: "app-dynamic-form-list",
    templateUrl: "./dynamic-form-list.component.html",
    styleUrls: ["./dynamic-form-list.component.scss"],
})
export class DynamicFormListComponent implements OnInit, OnDestroy {
    @Input() fromDescriptor: DynamicFormDescriptor;
    @Input() fromListValues: DynamicFromListValue[];

    @Output() dfChange: EventEmitter<DynamicForm[]>;

    formList: DynamicForm[];
    formRelations: DynamicyFormRealtion;
    formItemsSubscriptions: Subscription[];

    themeName: string;

    constructor(
        private themeService: NbThemeService,
        private confirmationService: ConfirmationService
    ) {
        this.dfChange = new EventEmitter<DynamicForm[]>();

        this.formList = [];
        this.formRelations = {};
        this.formItemsSubscriptions = [];
    }

    ngOnInit(): void {
        this.initFormRelations();

        if (this.fromListValues && this.fromListValues.length) {
            this.fromListValues.forEach((itemValue) => {
                this.onItemAdd(itemValue);
            });
        } else {
            this.onItemAdd();
        }

        this.themeService.onThemeChange().subscribe((theme: any) => {
            this.themeName = theme.name;
        });
    }

    ngOnDestroy(): void {
        this.formItemsSubscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        });

        this.confirmationService.closeModal();
    }

    initFormRelations(): void {
        const principals = Object.entries(this.fromDescriptor)
            .filter(([_, descriptor]) => descriptor.dependsOn)
            .map(([_, descriptor]) => descriptor.dependsOn.toString())
            .filter((key, index, array) => array.indexOf(key) === index);

        principals.forEach((principal) => {
            const dependants = Object.entries(this.fromDescriptor)
                .filter(
                    ([_, descriptor]) =>
                        descriptor.dependsOn && descriptor.dependsOn.toString() === principal
                )
                .map(
                    ([key, descriptor]): DynamicyFormDependent => {
                        return {
                            key: key,
                            typeFn: descriptor.type as DynamicFormItemTypeDependent,
                        };
                    }
                );

            this.formRelations[principal.toString()] = dependants;
        });
    }

    initFormSubscription(form: DynamicForm, formValue: DynamicFromListValue = {}): void {
        Object.entries(this.formRelations).forEach(([principal, dependants]) => {
            const itemSubscription = form.items[principal].valueChange
                .pipe(
                    startWith(form.value[principal]),
                    filter((value) => value !== undefined && value !== null),
                    distinctUntilChanged()
                )
                .subscribe((value: any) => {
                    const dependantItems: { [key: string]: DynamicFormItem } = {};

                    dependants.forEach((dependant) => {
                        const dependentType = dependant.typeFn(value);

                        dependantItems[dependant.key] = new dependentType({
                            label: this.fromDescriptor[dependant.key].label,
                            disabled: formValue[dependant.key]?.disbled || false,
                            value: formValue[dependant.key]?.value || null,
                            validators: this.fromDescriptor[dependant.key].validators,
                            asyncValidators: this.fromDescriptor[dependant.key].asyncValidators,
                            context: this.fromDescriptor[dependant.key].context(value),
                        });
                    });

                    form.setItems({
                        ...form.items,
                        ...dependantItems,
                    });
                });

            this.formItemsSubscriptions.push(itemSubscription);
        });
    }

    createForm(formValue: DynamicFromListValue = {}): DynamicForm {
        const formItems: { [key: string]: DynamicFormItem } = {};

        Object.entries(this.fromDescriptor)
            .filter(([key, _]) => !this.fromDescriptor[key].dependsOn)
            .forEach(([key, descriptor]) => {
                formItems[key] = this.createFormIndependentItem(key, descriptor, formValue[key]);
            });

        const form = new DynamicForm({ items: formItems });

        this.initFormSubscription(form, formValue);

        return form;
    }

    createFormIndependentItem(
        itemKey: string,
        itemDescriptor: DynamicFormItemDescriptor,
        itemValue?: DynamicFormListInnerItemValue
    ): DynamicFormItem {
        let independentType: DynamicFormItemTypeIndependent = null;
        independentType = itemDescriptor.type as DynamicFormItemTypeIndependent;

        return new independentType({
            label: this.fromDescriptor[itemKey].label,
            disabled: itemValue?.disbled || false,
            value: itemValue?.value || null,
            validators: this.fromDescriptor[itemKey].validators,
            asyncValidators: this.fromDescriptor[itemKey].asyncValidators,
            context: this.fromDescriptor[itemKey].context,
        });
    }

    removeFormItem(removeIndex: number): void {
        this.formList = this.formList.filter(
            (_: DynamicForm, index: number) => index !== removeIndex
        );

        this.dfChange.emit(this.formList);
    }

    onItemAdd(formValue?: DynamicFromListValue): void {
        const form = this.createForm(formValue);

        if (formValue) {
            const isDirty =
                Object.keys(formValue).length &&
                Object.values(formValue).some((value) => value !== undefined && value !== null);

            form.setStatus({
                ...form.status,
                dirty: isDirty,
            });
        }

        this.formList.push(form);
        this.dfChange.emit(this.formList);
    }

    onItemRemove(removeIndex: number): void {
        const removedFrom = this.formList[removeIndex];

        if (removedFrom.status.dirty) {
            this.confirmationService.openModal({
                title: `Remove From Item`,
                message: `Are you sure you want to remove item ?`,
                onConfirm: () => {
                    this.removeFormItem(removeIndex);
                },
            });
        } else {
            this.removeFormItem(removeIndex);
        }
    }
}

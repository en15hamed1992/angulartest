import { AbstractControl, ValidatorFn } from "@angular/forms";

export const PasswordValidator = (errorViewKey: string) => {
    const validator: ValidatorFn = (ctrl: AbstractControl) => {
        if (!ctrl.value) {
            return null;
        }

        const passwordRegex = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]))");
        if (passwordRegex.test(ctrl.value)) {
            return null;
        }

        return {
            passwoardError: getErrorMessage(ctrl.value),
        };
    };

    const getErrorMessage = (ctrlValue: string): string => {
        const message = `"${errorViewKey}" must must contains `;

        const hasCapitalsRegex = new RegExp("^(?=.*[A-Z])");
        const hasSmallsRegex = new RegExp("^(?=.*[a-z])");
        const hasDigitsRegex = new RegExp("^(?=.*[0-9])");

        if (
            !hasCapitalsRegex.test(ctrlValue) &&
            !hasSmallsRegex.test(ctrlValue) &&
            !hasDigitsRegex.test(ctrlValue)
        ) {
            return message.concat("capital letters, small letters and digits");
        }
        if (!hasCapitalsRegex.test(ctrlValue) && !hasSmallsRegex.test(ctrlValue)) {
            return message.concat("capital letters and small letters");
        }
        if (!hasCapitalsRegex.test(ctrlValue) && !hasDigitsRegex.test(ctrlValue)) {
            return message.concat("capital letters and digits");
        }
        if (!hasSmallsRegex.test(ctrlValue) && !hasDigitsRegex.test(ctrlValue)) {
            return message.concat("small letters and digits");
        }
        if (!hasCapitalsRegex.test(ctrlValue)) {
            return message.concat("capital letters");
        }
        if (!hasSmallsRegex.test(ctrlValue)) {
            return message.concat("small letters");
        }
        if (!hasDigitsRegex.test(ctrlValue)) {
            return message.concat("digits");
        }
    };

    return validator;
};

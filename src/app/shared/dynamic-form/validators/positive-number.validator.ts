import { AbstractControl, ValidatorFn } from "@angular/forms";

export const PositiveNumberValidator = (errorViewKey: string) => {
    const validator: ValidatorFn = (ctrl: AbstractControl) => {
        if (!ctrl.value) {
            return null;
        }

        if (!isNaN(parseInt(ctrl.value)) && parseInt(ctrl.value) < 0) {
            return {
                positiveNumberError: `"${errorViewKey}" must be greater than (0)`,
            };
        }

        return null;
    };

    return validator;
};

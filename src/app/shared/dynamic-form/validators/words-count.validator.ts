import { AbstractControl, ValidatorFn } from "@angular/forms";

export const WordsCountValidator = (count: number, type: WordsCountType, errorViewKey: string) => {
    const validator: ValidatorFn = (ctrl: AbstractControl) => {
        if (!ctrl.value) {
            return null;
        }

        const wordsCount = (ctrl.value as string).trim().split(" ").length;

        if (type === WordsCountType.MOST && wordsCount > count) {
            return {
                minSelectError: `"${errorViewKey}" must be "${count}" words at most`,
            };
        }

        if (type === WordsCountType.LEAST && wordsCount < count) {
            return {
                minSelectError: `"${errorViewKey}" must be "${count}" words at least`,
            };
        }

        return null;
    };

    return validator;
};

export enum WordsCountType {
    LEAST = 1,
    MOST = 2,
}

import { AbstractControl, ValidatorFn } from "@angular/forms";

export const UrlValidator = () => {
    const validator: ValidatorFn = (ctrl: AbstractControl) => {
        if (!ctrl.value) {
            return null;
        }

        const urlRegex = new RegExp(
            "^(https?://(?:www.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|www.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|https?://(?:www.|(?!www))[a-zA-Z0-9]+.[^s]{2,}|www.[a-zA-Z0-9]+.[^s]{2,})"
        );
        if (urlRegex.test(ctrl.value)) {
            return null;
        }

        return {
            passwoardError: "A valid URL is required",
        };
    };

    return validator;
};

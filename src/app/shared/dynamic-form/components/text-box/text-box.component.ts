import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { TextBoxContext } from "./models";

@Component({
    selector: "ngx-text-box",
    templateUrl: "./text-box.component.html",
    styleUrls: ["./text-box.component.scss"],
})
export class TextBoxComponent implements OnInit, DynamicFormItemComponent<TextBoxContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<TextBoxContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: FormControl;
    context: TextBoxContext;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control as FormControl;
        this.context = this.itemObj.context;
    }
}

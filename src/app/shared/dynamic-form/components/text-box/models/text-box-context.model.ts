import { TextBoxTypes } from ".";

export interface TextBoxContext {
    type: TextBoxTypes;
    placeholder: string;
}

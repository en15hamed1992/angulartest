import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { distinctUntilChanged, map } from "rxjs/operators";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { NumberInputContext } from "./models";

@Component({
    selector: "ngx-number-input",
    templateUrl: "./number-input.component.html",
    styleUrls: ["./number-input.component.scss"],
})
export class NumberInputComponent implements OnInit, DynamicFormItemComponent<NumberInputContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<NumberInputContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: FormControl;
    context: NumberInputContext;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control as FormControl;
        this.context = this.itemObj.context;

        this.itemObj.valueChange
            .pipe(
                map((value: any) => {
                    if (!isNaN(value)) {
                        return parseInt(value);
                    }
                    return null;
                }),
                distinctUntilChanged((prev: any, curr: any) => {
                    if (curr) {
                        return prev === curr;
                    }
                    return true;
                })
            )
            .subscribe((value: any) => {
                this.control.setValue(value);
            });
    }
}

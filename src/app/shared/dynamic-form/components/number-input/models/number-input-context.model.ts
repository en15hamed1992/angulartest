export interface NumberInputContext {
    step?: number;
    placeholder: string;
}

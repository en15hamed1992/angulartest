import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormGroup } from "@angular/forms";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { DateRangeContext } from "./models";

@Component({
    selector: "ngx-date-range",
    templateUrl: "./date-range.component.html",
    styleUrls: ["./date-range.component.scss"],
})
export class DateRangeComponent implements OnInit, DynamicFormItemComponent<DateRangeContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<DateRangeContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: AbstractControl;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control;
    }
}

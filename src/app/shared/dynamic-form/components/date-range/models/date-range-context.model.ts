export interface DateRangeContext {
    start: Date;
    end: Date;
}

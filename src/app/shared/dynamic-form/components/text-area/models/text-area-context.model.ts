export interface TextAreaContext {
    rows?: number;
    placeholder: string;
}

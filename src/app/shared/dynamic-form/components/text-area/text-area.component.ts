import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormGroup } from "@angular/forms";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { TextAreaContext } from "./models";

@Component({
    selector: "ngx-text-area",
    templateUrl: "./text-area.component.html",
    styleUrls: ["./text-area.component.scss"],
})
export class TextAreaComponent implements OnInit, DynamicFormItemComponent<TextAreaContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<TextAreaContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: AbstractControl;
    context: TextAreaContext;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control;
        this.context = this.itemObj.context;
    }
}

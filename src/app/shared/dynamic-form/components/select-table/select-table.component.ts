import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { NbDialogRef, NbDialogService } from "@nebular/theme";

import { Status } from "../../../../@theme/constants/status";
import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { SelectComponent, SelectTableContext } from "./models";

@Component({
    selector: "ngx-select-table",
    templateUrl: "./select-table.component.html",
    styleUrls: ["./select-table.component.scss"],
})
export class SelectTableComponent implements OnInit, DynamicFormItemComponent<SelectTableContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<SelectTableContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: FormControl;
    context: SelectTableContext;

    selectedItems: any | any[];
    modalBtnText: string;
    dialog: NbDialogRef<SelectComponent>;
    spinnerConfig = {
        bdColor: "transparent",
        color: Status.PRIMARY,
        type: "ball-clip-rotate",
        size: "default",
        fullScreen: false,
    };

    constructor(private dialogService: NbDialogService) {}

    ngOnInit(): void {
        this.control = this.itemObj.control as FormControl;
        this.itemObj.contextChange.subscribe((context: SelectTableContext) => {
            this.context = context;
        });

        if (this.itemObj.value) {
            this.selectedItems = this.itemObj.value;
        }

        this.setModalBtnText();
    }

    open() {
        this.dialog = this.dialogService.open(this.context.renderComponent, {
            context: {
                modal: true,
                params: this.context.params,
                multiSelect: this.context.multiSelect,
                preSelected: this.selectedItems,
                selectFn: (selectedItems: any | any[]) => {
                    this.selectedItems = selectedItems;
                    this.setModalBtnText();
                },
                closeFn: () => {
                    this.dialog.close();
                },
            },
            hasScroll: true,
            dialogClass: "table-modal",
        });

        this.dialog.onClose.subscribe(() => {
            this.control.markAsTouched();

            this.control.setValue(this.selectedItems);
        });
    }

    setModalBtnText(): void {
        if (!this.itemObj.context.multiSelect) {
            this.modalBtnText = this.selectedItems
                ? this.selectedItems[this.itemObj.context.viewKey]
                : "select_one_item";
        } else {
            this.modalBtnText = this.selectedItems?.length
                ? `${this.selectedItems?.length} items selected`
                : "zero_selected_items";
        }
    }

    hasValue(): boolean {
        if (
            (!this.itemObj.context.multiSelect && !this.selectedItems) ||
            (this.itemObj.context.multiSelect && !this.selectedItems?.length)
        ) {
            return false;
        }
        return true;
    }
}

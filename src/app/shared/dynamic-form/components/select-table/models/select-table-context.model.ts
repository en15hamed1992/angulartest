import { Type } from "@angular/core";
import { SelectComponent } from ".";

export interface SelectTableContext<T = any> {
    params?: any;
    multiSelect: boolean;
    viewKey: string;
    renderComponent: Type<SelectComponent<T>>;
}

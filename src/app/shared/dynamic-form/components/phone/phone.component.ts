import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormGroup } from "@angular/forms";
import { CountryISO, SearchCountryField, TooltipLabel } from "ngx-intl-tel-input";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { PhoneContext } from "./models";

@Component({
    selector: "ngx-phone",
    templateUrl: "./phone.component.html",
    styleUrls: ["./phone.component.scss"],
})
export class PhoneComponent implements OnInit, DynamicFormItemComponent {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<PhoneContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: AbstractControl;
    context: PhoneContext;

    CountryISO = CountryISO;
    SearchCountryField = SearchCountryField;
    TooltipLabel = TooltipLabel;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control;
        this.context = this.itemObj.context;
    }
}

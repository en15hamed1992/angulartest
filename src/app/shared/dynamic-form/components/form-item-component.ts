import { FormGroup } from "@angular/forms";

export interface FormItemComponenet {
    group: FormGroup;
    formKey: string;
    itemKey: string;
}

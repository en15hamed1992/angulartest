import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { DateContext } from "./models";

@Component({
    selector: "ngx-date",
    templateUrl: "./date.component.html",
    styleUrls: ["./date.component.scss"],
})
export class DateComponent implements OnInit, OnDestroy, DynamicFormItemComponent<DateContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<any>;
    @Input("parentControl") parentControl: FormGroup;

    control: FormControl;
    context: DateContext;

    contextSubscription: Subscription;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control as FormControl;

        this.contextSubscription = this.itemObj.contextChange.subscribe((context: DateContext) => {
            this.context = context;
        });
    }

    ngOnDestroy(): void {
        this.contextSubscription.unsubscribe();
    }
}

import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormGroup } from "@angular/forms";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { CheckboxContext } from "./models";

@Component({
    selector: "ngx-checkbox",
    templateUrl: "./checkbox.component.html",
    styleUrls: ["./checkbox.component.scss"],
})
export class CheckboxComponent implements OnInit, DynamicFormItemComponent<CheckboxContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<CheckboxContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: AbstractControl;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control;
    }

    onCheckChange(eventData: any): void {
        if (eventData.target.checked) {
            this.control.setValue(this.control.value);
        } else {
            this.control.setValue(null);
        }
    }
}

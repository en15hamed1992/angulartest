export interface DropdownContext {
    placeholder: string;
    options: {
        value: any;
        title: string;
    }[];
}

import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormGroup } from "@angular/forms";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { DropdownContext } from "./models";

@Component({
    selector: "ngx-dropdown",
    templateUrl: "./dropdown.component.html",
    styleUrls: ["./dropdown.component.scss"],
})
export class DropdownComponent implements OnInit, DynamicFormItemComponent<DropdownContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<DropdownContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: AbstractControl;

    placeholderView: string;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control;

        const selectedOption = this.itemObj.context.options.find(
            (option: { title: string; value: any }) => {
                if (option.value === this.itemObj.value) {
                    return option;
                }
            }
        );
        this.placeholderView = selectedOption
            ? selectedOption.title
            : this.itemObj.context.placeholder;
    }
}

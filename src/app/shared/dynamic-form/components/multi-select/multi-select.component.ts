import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormGroup } from "@angular/forms";

import { DynamicFormItem, DynamicFormItemComponent } from "../../models";
import { MultiSelectContext } from "./models";

@Component({
    selector: "ngx-multi-select",
    templateUrl: "./multi-select.component.html",
    styleUrls: ["./multi-select.component.scss"],
})
export class MultiSelectComponent implements OnInit, DynamicFormItemComponent<MultiSelectContext> {
    @Input("key") key: string;
    @Input("itemObj") itemObj: DynamicFormItem<MultiSelectContext>;
    @Input("parentControl") parentControl: FormGroup;

    control: AbstractControl;

    constructor() {}

    ngOnInit(): void {
        this.control = this.itemObj.control;
    }
}

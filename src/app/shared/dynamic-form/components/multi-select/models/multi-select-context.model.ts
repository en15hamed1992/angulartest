export interface MultiSelectContext {
    placeholder: string;
    options: {
        value: any;
        view: string;
    }[];
}

import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors } from '@angular/forms';

import { DynamicFormItem, DynamicFormItemComponent } from '../../models';
import { CsvUploaderContext } from './models';

@Component({
  selector: 'ngx-csv-uploader',
  templateUrl: './csv-uploader.component.html',
  styleUrls: ['./csv-uploader.component.scss'],
})
export class CsvUploaderComponent implements OnInit, DynamicFormItemComponent<CsvUploaderContext> {
  @ViewChild('uploadBtn', { static: false })
  uploadBtn: ElementRef<HTMLInputElement>;
  @ViewChild('csvInput', { static: false })
  csvInput: ElementRef<HTMLInputElement>;

  @Input('key') key: string;
  @Input('itemObj') itemObj: DynamicFormItem<CsvUploaderContext>;
  @Input('parentControl') parentControl: FormGroup;

  control: FormControl;
  context: CsvUploaderContext;

  btnText: string;

  loading: boolean;
  uploaded: boolean;
  entered: boolean;
  fileCompatible: boolean;

  constructor() {}

  ngOnInit(): void {
    this.control = this.itemObj.control as FormControl;
    this.btnText = this.itemObj.context.placeholder;
  }

  onUploadBtnClick(): void {
    this.csvInput.nativeElement.click();
  }

  onUploadBtnFocus(): void {
    if (!this.entered) {
      this.entered = true;
    } else {
      this.control.markAsTouched();
    }
  }

  onCsvUpload(eventData: Event): void {
    const file = (eventData.target as HTMLInputElement).files[0];
    const fileReader = new FileReader();

    this.btnText = file ? file.name : this.btnText;

    if (file) {
      fileReader.readAsText(file);
    }

    fileReader.onload = (loadEvent: ProgressEvent<FileReader>) => {
      this.loading = true;

      const fileContent: string = loadEvent.target.result as string;
      const delimiter: string = ',';

      const data: any[] = this.convertCsv(fileContent, delimiter);

      this.setForm(data);

      this.loading = false;
      this.uploaded = true;
    };
  }

  convertCsv(fileContent: string, delimiter: string): any[] {
    const fileTitles = fileContent.slice(0, fileContent.indexOf('\r\n')).split(delimiter);

    this.checkCompatibilty(fileTitles);

    const fileRows: any = fileContent
      .slice(fileContent.indexOf('\n') + 1)
      .split('\r\n')
      .filter((row: string) => {
        return row;
      });

    const data: any[] = fileRows.map((row: string) => {
      const values: string[] = row.split(delimiter);

      return fileTitles.reduce((acc: any, curr: string, index: number) => {
        acc[curr] = values[index];
        return acc;
      }, {});
    });

    return data;
  }

  setForm(data: any[]): void {
    const formValue: any[] = [];

    data.forEach((item: any) => {
      const csvRow: any = {};

      Object.entries(item).forEach((entry: [string, any]) => {
        const [key, value] = entry;

        if (this.itemObj.context.columns.map((col) => col.key).includes(key)) {
          csvRow[key] = value;
        }
      });

      formValue.push(csvRow);
    });

    if (this.fileCompatible) {
      this.control.setValue(formValue);
    } else {
      this.control.setErrors({
        compatibiltyError: `File is not compatible with pattern ${this.stringColumns(
          this.itemObj.context.columns
        )}`,
      });
    }
  }

  checkCompatibilty(titles: string[]): void {
    this.fileCompatible = true;

    this.itemObj.context.columns
      .filter((col) => col.required)
      .forEach((col) => {
        if (!titles.includes(col.key)) {
          this.fileCompatible = false;
        }
      });
  }

  stringColumns(columns: { key: string; required: boolean }[]): string {
    let result = '[ ';
    columns.forEach(
      (
        columns: { key: string; required: boolean },
        index: number,
        array: { key: string; required: boolean }[]
      ) => {
        result = result.concat(columns.key);
        result = index < array.length - 1 ? result.concat(' - ') : result.concat(' ]');
      }
    );

    return result;
  }

  onResetForm(): void {
    this.uploaded = false;
    this.btnText = this.itemObj.context.placeholder;
    this.csvInput.nativeElement.value = '';

    this.control.setValue(null);
  }
}

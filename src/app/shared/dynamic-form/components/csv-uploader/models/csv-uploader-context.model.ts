export interface CsvUploaderContext {
    columns: { key: string; required: boolean }[];
    placeholder: string;
}

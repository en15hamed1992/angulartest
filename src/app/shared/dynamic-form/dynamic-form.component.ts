import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ComponentFactoryResolver,
  ViewChild,
  ChangeDetectorRef,
  AfterViewChecked,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
} from "@angular/forms";

import { ItemHostDirective } from "./directives/item-host.directive";
import { DynamicForm, DynamicFormItemComponent } from "./models";

@Component({
  selector: "dynamic-form",
  templateUrl: "./dynamic-form.component.html",
  styleUrls: ["./dynamic-form.component.scss"],
})
export class DynamicFormComponent implements OnInit, AfterViewChecked {

  @ViewChild(ItemHostDirective, { static: true })
  ngxItemHost: ItemHostDirective;

  @Input() formObj: DynamicForm;
  @Input() disabled: boolean = false;
  @Input() hideSubmitButton: boolean;
  @Input() submitButtonTextContent: string;
  @Input() transformFn: (values: any) => any;

  @Output() dfSubmit: EventEmitter<any>;

  form: FormGroup;
  formValus: any;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder
  ) {
    this.dfSubmit = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.formValus = {};
    this.formObj.itemsChange.subscribe(() => {
      this.initForm();
    });
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();

    this.form.valueChanges.subscribe(() => {
      this.formObj.setStatus({
        valid: this.form.valid,
        dirty: this.form.dirty,
        touched: this.form.touched,
      });
    });
  }

  initForm(): void {
    this.form = this.formBuilder.group(
      {},
      {
        validators: this.formObj.validators,
        asyncValidators: this.formObj.asyncValidators,
      }
    );

    this.form.valueChanges.subscribe(() => {
      this.formObj.setStatus({
        valid: this.form.valid,
        dirty: this.form.dirty,
        touched: this.form.touched,
      });
    });

    this.formObj.statusChange.subscribe((status) => {
      status.dirty ? this.form.markAsDirty() : this.form.markAsPristine();
      status.touched ? this.form.markAsTouched() : this.form.markAsUntouched();
    });

    const viewContainerRef = this.ngxItemHost.viewContainerRef;
    viewContainerRef.clear();

    Object.keys(this.formObj.items).forEach((key: string) => {
      this.form.addControl(key, this.formObj.items[key].control);
    });

    Object.keys(this.formObj.items).forEach((key: string) => {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
        this.formObj.items[key].renderComponent
      );
      
      const componentRef = viewContainerRef.createComponent<DynamicFormItemComponent>(
        componentFactory
      );

      componentRef.instance.key = key;
      componentRef.instance.itemObj = this.formObj.items[key];
      componentRef.instance.parentControl = this.form;
    });
  }

  onSubmit(values: any): void {
    const errors: any[] = this.checkFormErrors(this.form);

    if (errors.length > 0) {
      console.log(errors);
    } else {
      const excludedValue = this.excludeEmptyProps(values.main);

      if (this.form.valid) {
        this.dfSubmit.emit(excludedValue);
      }
    }
  }

  resetForm(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    try {
      Object.keys(this.formObj).forEach((key: string) => {
        this.form.get(key).reset();
      });
    } catch (error) {
      console.error(error);
    }
  }

  private excludeEmptyProps(obj: any): any {
    let newObj: any = {};

    Object.entries(obj).forEach((formValue: [string, any]) => {
      const [key, value] = formValue;

      if (value !== null && value !== undefined && value != "") {
        newObj[key] = value;
      }
    });

    return newObj;
  }

  private checkFormErrors(form: FormGroup): any[] {
    const controlErrors: any[] = [];

    Object.entries(form.controls).forEach(
      (control: [string, AbstractControl]) => {
        const [key, value] = control;
        const errors: ValidationErrors = this.form.get(key).errors;

        if (errors) {
          Object.entries(errors).forEach((error: [string, any]) => {
            const errorKey: string = error[0];
            const errorValue: any = error[1];

            controlErrors.push({
              control: key,
              value: value,
              error: {
                key: errorKey,
                value: errorValue,
              },
            });
          });
        }
      }
    );

    return controlErrors;
  }
}

export interface DynamicFormListInnerItemValue {
    disbled: false;
    value: any;
}

export type DynamicFromListValue = { [key: string]: DynamicFormListInnerItemValue };

import { AsyncValidatorFn, ValidatorFn } from "@angular/forms";
import { DynamicFormItem } from ".";
import { DynamicBaseState } from "../items";

export interface DynamicFormItemDescriptor<TContext = any> {
    label: string;
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    dependsOn: string | false;
    type: DynamicFormItemType<TContext>;
    context: DynamicFormItemContext<TContext>;
}

export type DynamicFormDescriptor = { [key: string]: DynamicFormItemDescriptor };

export type DynamicFormItemTypeIndependent<TContext = any> = {
    new (state: DynamicBaseState): DynamicFormItem<TContext>;
};

export type DynamicFormItemTypeDependent<TContext = any> = (
    formValues: string
) => DynamicFormItemTypeIndependent<TContext>;

export type DynamicFormItemType<TContext = any> =
    | DynamicFormItemTypeDependent<TContext>
    | DynamicFormItemTypeIndependent<TContext>;

export type DynamicFormItemContext<TContext = any> = TContext | ((values: any) => TContext);

export type DynamicyFormDependent<TContext = any> = {
    key: string;
    typeFn: DynamicFormItemTypeDependent<TContext>;
};

export type DynamicyFormRealtion<TContext = any> = {
    [principal: string]: DynamicyFormDependent<TContext>[];
};

import { Type } from "@angular/core";
import { AbstractControl, AsyncValidatorFn, ValidatorFn } from "@angular/forms";
import { BehaviorSubject, Observable } from "rxjs";

import { DynamicFormItemComponent, DynamicFormItemState } from ".";

export abstract class DynamicFormItem<TContext = any> {
    private readonly _control: AbstractControl;
    private readonly _validators?: ValidatorFn[];
    private readonly _asyncValidators?: AsyncValidatorFn[];
    private readonly _renderComponent: Type<DynamicFormItemComponent<TContext>>;

    private readonly _label$: BehaviorSubject<string>;
    private readonly _context$: BehaviorSubject<TContext>;

    constructor(state: DynamicFormItemState<TContext>) {
        this._control = state.control;

        this._control.setValue(state.value);
        this._control.setValidators(state.validators);
        this._control.setAsyncValidators(state.asyncValidators);

        state.disabled ? this.control.disable() : this.control.enable();

        this._validators = state.validators;
        this._asyncValidators = state.asyncValidators;

        this._label$ = new BehaviorSubject<string>(state.label);
        this._context$ = new BehaviorSubject<TContext>(state.context || null);

        this._renderComponent = state.renderComponent;
    }

    get control(): AbstractControl {
        return this._control;
    }

    get label(): string {
        return this._label$.getValue();
    }

    get disabled(): boolean {
        return this._control.disabled;
    }

    get value(): any {
        return this._control.value;
    }

    get validators(): ValidatorFn[] {
        return this._validators;
    }

    get asyncValidators(): AsyncValidatorFn[] {
        return this._asyncValidators;
    }

    get context(): TContext {
        return this._context$.getValue();
    }

    get renderComponent(): Type<DynamicFormItemComponent> {
        return this._renderComponent;
    }

    get labelChange(): Observable<string> {
        return this._label$.asObservable();
    }

    get valueChange(): Observable<any> {
        return this._control.valueChanges;
    }

    get contextChange(): Observable<TContext> {
        return this._context$.asObservable();
    }

    setLabel(label: string): void {
        this._label$.next(label);
    }

    setDisabled(disabled: boolean): void {
        if (disabled) {
            this.control.disable();
        } else {
            this.control.enable();
        }
    }

    setValue(value: any): void {
        this._control.setValue(value);
    }

    setContext(context: TContext): void {
        this._context$.next(context);
    }

    reset(): void {
        this._control.reset();
    }
}

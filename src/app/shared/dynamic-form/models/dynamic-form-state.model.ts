import { AsyncValidatorFn, ValidatorFn } from "@angular/forms";

export interface DynamicFormState {
    validators?: ValidatorFn[];
    asyncValidators?: AsyncValidatorFn[];
    items: {
        [key: string]: any;
    };
}

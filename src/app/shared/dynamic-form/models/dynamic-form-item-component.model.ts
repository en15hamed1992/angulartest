import { FormGroup } from "@angular/forms";
import { DynamicFormItem } from "./dynamic-form-item.model";

export interface DynamicFormItemComponent<TContext = any> {
    key: string;
    itemObj: DynamicFormItem<TContext>;
    parentControl: FormGroup;
}

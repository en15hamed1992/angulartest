import { Type } from "@angular/core";
import { AbstractControl, AsyncValidatorFn, ValidatorFn } from "@angular/forms";
import { DynamicFormItemComponent } from ".";

export interface DynamicFormItemState<TContext = any> {
    control: AbstractControl;

    label: string;
    disabled: boolean;
    value: any;
    validators: ValidatorFn[];
    asyncValidators: AsyncValidatorFn[];
    context: TContext;

    renderComponent: Type<DynamicFormItemComponent<TContext>>;
}

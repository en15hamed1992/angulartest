import { DynamicForm } from "./dynamic-form.model";

export * from "./dynamic-form.model";
export * from "./dynamic-form-state.model";
export * from "./dynamic-form-status.model";

export * from "./dynamic-form-item.model";
export * from "./dynamic-form-item-state.model";
export * from "./dynamic-form-item-type.model";
export * from "./dynamic-form-item-component.model";

export * from "./dynamic-form-item-descriptor.model";
export * from "./dynamic-form-list-value.model";

export type SubmitButtonAssociations = { [key: string]: DynamicForm } | DynamicForm[];

export interface DynamicFormStatus {
    valid: boolean;
    invalid: boolean;
    dirty: boolean;
    pristine: boolean;
    touched: boolean;
    untouched: boolean;
}

export interface DynamicFormStatusSetter {
    valid?: boolean;
    dirty?: boolean;
    touched?: boolean;
}

import { AsyncValidatorFn, ValidatorFn } from "@angular/forms";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { distinctUntilChanged, startWith } from "rxjs/operators";

import { DynamicFormItem, DynamicFormState } from ".";
import { DynamicFormStatus, DynamicFormStatusSetter } from "./dynamic-form-status.model";

export class DynamicForm {
    private readonly _status$: BehaviorSubject<DynamicFormStatus>;
    private readonly _reset$: Subject<void>;

    private readonly _value$: BehaviorSubject<any>;
    private readonly _validators$: BehaviorSubject<ValidatorFn[]>;
    private readonly _asyncValidators$: BehaviorSubject<AsyncValidatorFn[]>;
    private readonly _items$: BehaviorSubject<{ [key: string]: DynamicFormItem }>;

    constructor(state: DynamicFormState) {
        this._status$ = new BehaviorSubject<DynamicFormStatus>({
            valid: false,
            invalid: true,
            dirty: false,
            pristine: true,
            touched: false,
            untouched: true,
        });
        this._reset$ = new Subject<void>();

        this._value$ = new BehaviorSubject<any>({});
        this._validators$ = new BehaviorSubject<ValidatorFn[]>(state.validators || []);
        this._asyncValidators$ = new BehaviorSubject<AsyncValidatorFn[]>(
            state.asyncValidators || []
        );
        this._items$ = new BehaviorSubject<{ [key: string]: any }>(state.items || {});
    }

    get value(): any {
        const formValue: any = {};

        Object.entries(this._items$.getValue()).forEach(
            ([key, item]: [string, DynamicFormItem]) => {
                formValue[key] = item.value;
            }
        );

        return formValue;
    }

    get items(): { [key: string]: DynamicFormItem } {
        return this._items$.getValue();
    }

    get status(): DynamicFormStatus {
        return this._status$.getValue();
    }

    get validators(): ValidatorFn[] {
        return this._validators$.getValue();
    }

    get asyncValidators(): AsyncValidatorFn[] {
        return this._asyncValidators$.getValue();
    }

    get valueChange(): Observable<any> {
        const formValues: any = {};

        this._items$.subscribe((items: { [key: string]: DynamicFormItem }) => {
            Object.entries(items).forEach(([key, item]: [string, DynamicFormItem]) => {
                formValues[key] = item.value;

                item.valueChange.subscribe((itemValue: any) => {
                    formValues[key] = itemValue;
                    this._value$.next(formValues);
                });
            });
        });

        return this._value$;
    }

    get statusChange(): Observable<DynamicFormStatus> {
        return this._status$.pipe(
            distinctUntilChanged((prev: DynamicFormStatus, curr: DynamicFormStatus) => {
                return (
                    prev.dirty === curr.dirty &&
                    prev.pristine === curr.pristine &&
                    prev.touched === curr.touched &&
                    prev.untouched === curr.untouched &&
                    prev.valid === curr.valid &&
                    prev.invalid === curr.invalid
                );
            })
        );
    }

    get itemsChange(): Observable<{ [key: string]: DynamicFormItem }> {
        return this._items$.asObservable();
    }

    reset(): void {
        this._reset$.next();
    }

    setStatus(status: DynamicFormStatusSetter): void {
        this._status$.next({
            valid: status.valid ? status.valid : this.status.valid,
            dirty: status.dirty ? status.dirty : this.status.dirty,
            touched: status.touched ? status.touched : this.status.touched,
            invalid: status.valid ? !status.valid : this.status.invalid,
            pristine: status.dirty ? !status.dirty : this.status.pristine,
            untouched: status.touched ? !status.touched : this.status.untouched,
        });
    }

    setValidators(validators: ValidatorFn[]): void {
        this._validators$.next(validators);
    }

    setAsyncValidators(asyncValidators: AsyncValidatorFn[]): void {
        this._asyncValidators$.next(asyncValidators);
    }

    setItems(items: { [key: string]: any }): void {
        this._items$.next(items);
    }
}

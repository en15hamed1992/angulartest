import { Validators } from "@angular/forms";
import { DynamicForm } from ".";

export interface DynamicFormModalContext {
    formObj: DynamicForm;
    title: string;
    submitTxt: string;
    validators: Validators;
    onSubmit: (result: any) => any;
}

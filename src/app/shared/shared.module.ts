import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NbEvaIconsModule } from "@nebular/eva-icons";
import {
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbSelectModule,
    NbAlertModule,
    NbDialogModule,
    NbTabsetModule,
    NbStepperModule,
    NbRadioModule,
    NbToggleModule,
    NbSpinnerModule,
    NbFormFieldModule,
    NbListModule,
    NbAccordionModule,
    NbTooltipModule,
    NbProgressBarModule,
} from "@nebular/theme";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { NgxSpinnerModule } from "ngx-spinner";

import { DashboardTableComponent } from "./dashboard-table/dashboard-table.component";
import { BooleanCellComponent } from "./boolean-cell/boolean-cell.component";
import { DateCellComponent } from "./date-cell/date-cell.component";
import { DynamicFormComponent } from "./dynamic-form/dynamic-form.component";
import { TranslatePipe } from "../translation/translate.pipe";
import { TranslateModule } from "../translation/translate/translate.module";
import { ColorPickerModule } from "ngx-color-picker";
import { NgxIntlTelInputModule } from "ngx-intl-tel-input";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { DynamicFormErrorsComponent } from "./dynamic-form-errors/dynamic-form-errors.component";
import { ConfirmationModalComponent } from "./confirmation-modal/confirmation-modal.component";
import { CustomListFilterComponent } from "./custom-list-filter/custom-list-filter.component";
import { CustomDateFilterComponent } from "./custom-date-filter/custom-date-filter.component";
import { PurchaseOrderStatusCellComponent } from "./purchase-order-status-cell/purchase-order-status-cell.component";
import { StatusCellComponent } from "./status-cell/status-cell.component";
import { DataViewComponent } from "./data-view/data-view.component";
import { CustomMinMaxDateFilterComponent } from "./custom-min-max-date-filter/custom-min-max-date-filter.component";
import { CustomMoreLessFilterComponent } from "./custom-more-less-filter/custom-more-less-filter.component";
import { TextBoxComponent } from "./dynamic-form/components/text-box/text-box.component";
import { CheckboxComponent } from "./dynamic-form/components/checkbox/checkbox.component";
import { TextAreaComponent } from "./dynamic-form/components/text-area/text-area.component";
import { DateComponent } from "./dynamic-form/components/date/date.component";
import { DateRangeComponent } from "./dynamic-form/components/date-range/date-range.component";
import { DropdownComponent } from "./dynamic-form/components/dropdown/dropdown.component";
import { MultiSelectComponent } from "./dynamic-form/components/multi-select/multi-select.component";
import { PhoneComponent } from "./dynamic-form/components/phone/phone.component";
import { ItemHostDirective } from "./dynamic-form/directives/item-host.directive";
import { CustomMultiSelectFilterComponent } from "./custom-select-filter/custom-select-filter.component";
import { CsvUploaderComponent } from "./dynamic-form/components/csv-uploader/csv-uploader.component";
import { DynamicFormModalComponent } from "./dynamic-form/dynamic-form-modal/dynamic-form-modal.component";
import { LoadingModalComponent } from "./loading-modal/loading-modal.component";
import { ErrorModalComponent } from "./error-modal/error-modal.component";
import { DataViewFieldComponent } from "./data-view/data-view-field/data-view-field.component";
import { ViewHostDirective } from "./data-view/directives/view-host.directive";
import { ActivationFieldComponent } from "./data-view/components/activation-field/activation-field.component";
import { DateFieldComponent } from "./data-view/components/date-field/date-field.component";
import { ImageFieldComponent } from "./data-view/components/image-field/image-field.component";
import { SelectTableComponent } from "./dynamic-form/components/select-table/select-table.component";
import { NumberInputComponent } from "./dynamic-form/components/number-input/number-input.component";
import { LinkFieldComponent } from "./data-view/components/link-field/link-field.component";
import { DynamicFormButtonComponent } from "./dynamic-form/dynamic-form-button/dynamic-form-button.component";
import { DynamicFormListComponent } from "./dynamic-form/dynamic-form-list/dynamic-form-list.component";
import { ProgressBarModalComponent } from "./progress-bar-modal/progress-bar-modal.component";
import { ImageModalFieldComponent } from './data-view/components/image-modal-field/image-modal-field.component';
import { CustomSelectTableFilterComponent } from "./custom-select-table-filter/custom-select-table-filter.component";
import { CustomNumberFilterComponent } from "./custom-number-filter/custom-number-filter";

@NgModule({
    declarations: [
        ActivationFieldComponent,

        BooleanCellComponent,

        CheckboxComponent,
        CustomDateFilterComponent,
        CustomListFilterComponent,
        CustomMinMaxDateFilterComponent,
        CustomMoreLessFilterComponent,
        CustomMultiSelectFilterComponent,
        CustomSelectTableFilterComponent,
        CustomNumberFilterComponent,
        ConfirmationModalComponent,
        CsvUploaderComponent,

        ErrorModalComponent,

        DataViewComponent,
        DataViewFieldComponent,
        DateComponent,
        DateFieldComponent,
        DateRangeComponent,
        DropdownComponent,
        DashboardTableComponent,
        DateCellComponent,
        DynamicFormComponent,
        DynamicFormComponent,
        DynamicFormErrorsComponent,
        DynamicFormModalComponent,

        ImageFieldComponent,
        LoadingModalComponent,

        PhoneComponent,

        MultiSelectComponent,

        PurchaseOrderStatusCellComponent,

        StatusCellComponent,

        TextBoxComponent,
        TextAreaComponent,

        ItemHostDirective,

        ViewHostDirective,

        SelectTableComponent,

        NumberInputComponent,

        LinkFieldComponent,

        DynamicFormButtonComponent,

        DynamicFormListComponent,

        ProgressBarModalComponent,

        ImageModalFieldComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        NbAccordionModule,
        NbAlertModule,
        NbButtonModule,
        NbCardModule,
        NbCheckboxModule,
        NbEvaIconsModule,
        NbDatepickerModule,
        NbDialogModule,
        NbFormFieldModule,
        NbIconModule,
        NbInputModule,
        NbListModule,
        NbProgressBarModule,
        NbRadioModule,
        NbSelectModule,
        NbSpinnerModule,
        NbStepperModule,
        NbTabsetModule,
        NbToggleModule,
        NbTooltipModule,

        InfiniteScrollModule,

        Ng2SmartTableModule,
        NgxIntlTelInputModule,
        NgxSpinnerModule,

        ColorPickerModule,

        TranslateModule,

        BsDropdownModule.forRoot(),
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,

        NbAccordionModule,
        NbAlertModule,
        NbButtonModule,
        NbCardModule,
        NbCheckboxModule,
        NbEvaIconsModule,
        NbDatepickerModule,
        NbDialogModule,
        NbFormFieldModule,
        NbIconModule,
        NbInputModule,
        NbListModule,
        NbProgressBarModule,
        NbRadioModule,
        NbSelectModule,
        NbSpinnerModule,
        NbStepperModule,
        NbTabsetModule,
        NbToggleModule,
        NbTooltipModule,

        InfiniteScrollModule,

        Ng2SmartTableModule,
        NgxSpinnerModule,

        DashboardTableComponent,
        DataViewComponent,
        DynamicFormListComponent,
        DynamicFormButtonComponent,
        DynamicFormComponent,

        TranslatePipe,
    ],
})
export class SharedModule {}

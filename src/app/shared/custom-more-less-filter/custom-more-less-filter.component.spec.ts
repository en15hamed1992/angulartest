import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CustomMoreLessFilterComponent } from "./custom-more-less-filter.component";

describe("CustomMoreLessFilterComponent", () => {
    let component: CustomMoreLessFilterComponent;
    let fixture: ComponentFixture<CustomMoreLessFilterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CustomMoreLessFilterComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustomMoreLessFilterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

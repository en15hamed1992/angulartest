import { Component, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { debounceTime, distinctUntilChanged, filter } from "rxjs/operators";
import { DefaultFilter } from "ng2-smart-table";
import { TranslateService } from "../../translation/translate.service";
import { NbLayoutDirectionService } from "@nebular/theme";

@Component({
  selector: "ngx-custom-more-less-filter",
  templateUrl: "./custom-more-less-filter.component.html",
  styleUrls: ["./custom-more-less-filter.component.scss"],
})
export class CustomMoreLessFilterComponent
  extends DefaultFilter
  implements OnInit, OnChanges {
  moreInputControl: FormControl;
  lessInputControl: FormControl;

  morePlaceholder: string;
  lessPlaceholder: string;

  constructor(
    private directionService: NbLayoutDirectionService,
    private translateService: TranslateService
  ) {
    super();

    this.moreInputControl = new FormControl();
    this.lessInputControl = new FormControl();
  }

  ngOnInit(): void {
    this.directionService.onDirectionChange().subscribe(() => {
      this.morePlaceholder = this.translateService.translate(
        this.column.filter.config.more.placeholder
      );
      this.lessPlaceholder = this.translateService.translate(
        this.column.filter.config.less.placeholder
      );
    });
    
    this.moreInputControl.valueChanges
      .pipe(
        // filter((value: any) => value !== null),
        debounceTime(400),
        distinctUntilChanged()

      )
      .subscribe(
        (value: any) => {
          const prevColomunId = this.column.id;
          this.column.id = this.column.filter.config.more.id;

          this.query = isFinite(value) ? (value + 0).toString() : "";
          this.setFilter();

          this.column.id = prevColomunId;
        },
        (err: any) => {
          console.error(err);
        }
      );

    this.lessInputControl.valueChanges
      .pipe(
        // filter((value) => value != null && !isNaN(value)),
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe(
        (value: any) => {
          const prevColomunId = this.column.id;
          this.column.id = this.column.filter.config.less.id;

          this.query = isFinite(value) ? (value + 0).toString() : "";

          this.setFilter();

          this.column.id = prevColomunId;
        },
        (err: any) => {
          console.error(err);
        }
      );
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log({changes: changes});
    if (changes.query) {
      this.query = changes.query.currentValue;
      if(changes.query.currentValue =='')this.resetFilter();
    }
  }

  resetFilter() {
    this.query = "";
    this.moreInputControl.setValue(null, { emitEvent: false });
    this.lessInputControl.setValue(null, { emitEvent: false });
    this.setFilter();
  }
}

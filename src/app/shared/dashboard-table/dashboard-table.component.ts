import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  OnDestroy,
  ViewChild,
  SimpleChanges,
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Ng2SmartTableComponent } from "ng2-smart-table";
import { BehaviorSubject, Subject, Subscription } from "rxjs";
import { switchMap, tap } from "rxjs/operators";

import { ModifiedServerDataSource } from "./modified-server-data-source";
import { DashboardTableModifiers } from "./dashboard-table-modifiers";
import { RoleProviderService } from "./../../services/role-provider.service";
import {
  DashboardTableAction,
  DashboardTableActions,
  DashboardTableColumn,
  DashboardTableColumsFilter,
  DashboardTableFilter,
} from "./models";
import { ServerSourceData } from "./models";
import { DashboardTableColumns } from "./models/dashboard-table-columns.mode";
import { TranslateService } from "../../translation/translate.service";
import { ApiServiceInterface } from "../../interfaces/api-service.interface";
import { NbLayoutDirectionService } from "@nebular/theme";

@Component({
  selector: "dashboard-table",
  templateUrl: "./dashboard-table.component.html",
  styleUrls: ["./dashboard-table.component.scss"],
})
export class DashboardTableComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild("table", { static: false }) table: Ng2SmartTableComponent;

  @Input() sourceService: ApiServiceInterface;
  @Input() sortFieldValueMap: { [key: string]: string };
  //   @Input() endpoint: string;
  @Input() params: any;
  @Input() dataKey: string;
  @Input() actions: DashboardTableAction[] = [];
  @Input() columns: DashboardTableColumns;
  @Input() filters: DashboardTableFilter[];
  @Input() pageLimit: number = 50;
  @Input() enableSingleSelection: boolean;
  @Input() enableMultiSelection: boolean;
  @Input() preSelected: any | any[];

  @Output() dtAdd = new EventEmitter<any>();
  @Output() dtEdit = new EventEmitter<any>();
  @Output() dtDelete = new EventEmitter<any>();
  @Output() dtView = new EventEmitter<any>();
  @Output() dtLoad = new EventEmitter<void>();
  @Output() dtCreate = new EventEmitter<DashboardTableModifiers>();
  @Output() dtSelect = new EventEmitter<any | any[]>();

  selections: any;

  serverSource: ModifiedServerDataSource;
  clientSource: ClientData;

  settings: any;

  modifiers: DashboardTableModifiers;

  private _resetFilters$: Subject<void>;
  private _refreshData$: Subject<void>;
  private _preSelected$: BehaviorSubject<any | any[]>;
  private _subscription$: Subscription = new Subscription();

  constructor(
    private directionService: NbLayoutDirectionService,
    private roleProviderService: RoleProviderService,
    private translateService: TranslateService
  ) {
    this._resetFilters$ = new Subject<void>();
    this._refreshData$ = new Subject<void>();

    this.modifiers = new DashboardTableModifiers({
      resetFilters: this._resetFilters$,
      refreshData: this._refreshData$,
    });

    this._preSelected$ = new BehaviorSubject<any | any[]>(null);
  }

  ngOnInit(): void {
    this.dtCreate.emit(this.modifiers);

    this._subscription$.add(
      this._resetFilters$.subscribe(() => {
        this.serverSource.setFilter([]);
        this.serverSource.refresh();
      })
    );

    this._subscription$.add(
      this._refreshData$.subscribe(() => {
        this.serverSource.refresh();
      })
    );

    this._subscription$.add(
      this.directionService.onDirectionChange().subscribe(() => {
        this.settings = this.initSettings();
      })
    );

    this.serverSource = this.initServerSource();

    this.initServerSourceDataSubscription();

    if (!this.checkActions()) {
      this.settings.actions = false;
    }

    if (this.enableSingleSelection) {
      this.selections = null;
    }
    if (this.enableMultiSelection) {
      this.selections = {};
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.columns && changes.columns.currentValue) {
      this.settings = {
        ...this.settings,
        columns: changes.columns.currentValue,
      };
    }

    if (changes.preSelected) {
      this._preSelected$.next(changes.preSelected.currentValue);
    }
  }

  ngOnDestroy(): void {
    this._subscription$.unsubscribe();
  }

  initSettings(): any {
    return {
      actions: {
        custom: [],
        add: false,
        edit: false,
        delete: false,
        ...this.actions,
      },
      mode: "external",
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      editButtonContent: '<i class="nb-edit"></i>',
      edit: {
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: this.translateColumnTitles(
        this.filterColumnsByPermission(this.columns)
      ),
      pager: {
        display: true,
        perPage: this.pageLimit,
      },
      rowClassFunction: (row: any) => {
        let cssClass = "";
        if (this.enableSingleSelection || this.enableMultiSelection) {
          cssClass += " select-item";
        }
        if (this.enableMultiSelection && this.selections[row.data.id]) {
          cssClass += " selected-row";
        }
        if (this.enableSingleSelection && this.selections?.id === row.data.id) {
          cssClass += " selected-row";
        }
        if (row.data.active != undefined && !row.data.active) {
          cssClass += " disabled-row";
        }
        return cssClass;
      },
    };
  }

  initServerSource(): ModifiedServerDataSource {
    return new ModifiedServerDataSource({
      sourceService: this.sourceService,
      sortFieldValueMap: this.sortFieldValueMap,
      params: this.params,
      dataKey: this.dataKey ? this.dataKey : "result",
      pagerPageKey: "page",
      sortFieldKey: "SortBy",
      sortDirKey: "SortDirction",
      pagerLimitKey: "limit",
      sortKey: "sort",
      filters: this.filters,
      totalKey: "pager.recordsCount",
    });
  }

  initServerSourceDataSubscription(): void {
    this._subscription$.add(
      this.serverSource
        .onChanged()
        .pipe(
          tap((data: ServerSourceData) => {
            if (data.action === "filter" && this.enableSingleSelection) {
              this._preSelected$.next(this.selections);
            }
            if (data.action === "filter" && this.enableMultiSelection) {
              this._preSelected$.next(
                Object.keys(this.selections).map(
                  (key: string) => this.selections[key]
                )
              );
            }

            setTimeout(() => {
              this.dtLoad.emit();
              this.table.grid.dataSet.deselectAll();
            });
          }),
          switchMap(() => this._preSelected$)
        )
        .subscribe((values: any | any[]) => {
          setTimeout(() => {
            this.setPreSelectedRows(values);
          });
        })
    );
  }

  filterColumnsByPermission(
    columns: DashboardTableColumns
  ): DashboardTableColumns {
    const filteredColumns: DashboardTableColumns = {};

    Object.entries(columns).forEach(([key, value]: [string, any]) => {
      if (!value.permissions || !value.permissions.length) {
        filteredColumns[key] = value;
      } else if (this.roleProviderService.isGranted(value.permissions)) {
        filteredColumns[key] = value;
      }
    });

    return filteredColumns;
  }

  translateColumnTitles(columns: DashboardTableColumns): DashboardTableColumns {
    const translatedColumns: DashboardTableColumns = {};

    Object.entries(columns).forEach(
      ([key, value]: [string, DashboardTableColumn]) => {
        translatedColumns[key] = {
          ...value,
          title: this.translateService.translate(value.title),
        };
      }
    );

    return translatedColumns;
  }

  checkActions(): boolean {
    if (!this.actions) {
      return false;
    }

    let isThereAnAction = false;

    if (
      this.actions.map((x) => x.action).includes(DashboardTableActions.VIEW_ONE)
    ) {
      this.settings.actions["custom"].push({
        name: "view",
        title: '<i class="eva eva-eye-outline"></i>',
      });
      isThereAnAction = true;
    }

    if (
      this.actions.map((x) => x.action).includes(DashboardTableActions.UPDATE)
    ) {
      this.settings.actions["custom"].push({
        name: "edit",
        title: '<i class="eva eva-edit-outline"></i>',
      });
      isThereAnAction = true;
    }

    if (
      this.actions.map((x) => x.action).includes(DashboardTableActions.DELETE)
    ) {
      this.settings.actions["custom"].push({
        name: "delete",
        title: '<i class="eva eva-power-outline"></i>',
      });
      isThereAnAction = true;
    }

    if (
      this.actions.map((x) => x.action).includes(DashboardTableActions.CREATE)
    ) {
      this.settings.actions.add = true;
      isThereAnAction = true;
    }

    return isThereAnAction;
  }

  setPreSelectedRows(values: any | any[]): void {
    this.initSelections(values);

    this.table.grid.getRows().forEach((row) => {
      if (this.enableSingleSelection) {
        if (row.getData().id === values?.id) {
          this.table.grid.selectRow(row);
          this.selections = row.getData();
        }
      }
      if (this.enableMultiSelection) {
        if (
          (values as any[])?.map((value) => value.id).includes(row.getData().id)
        ) {
          this.table.grid.multipleSelectRow(row);
          this.selections[row.getData().id] = row.getData();
        } else {
          delete this.selections[row.getData().id];
        }
      }
    });
  }

  initSelections(values: any | any[]): void {
    const falsySelect: boolean =
      values === null || values === undefined || values === {} || values === [];

    if (falsySelect && this.enableSingleSelection) {
      this.selections = null;
    } else if (falsySelect && this.enableMultiSelection) {
      this.selections = {};
    } else {
      if (this.enableSingleSelection) {
        this.selections = values;
      }

      if (this.enableMultiSelection) {
        this.selections = {};
        (values as any[]).forEach((value: any) => {
          this.selections[value.id] = value;
        });
      }
    }
  }

  onCustomAction(event: any) {
    switch (event.action) {
      case "view":
        this.dtView.emit(event.data);
        break;

      case "edit":
        this.dtEdit.emit(event.data);
        break;

      case "delete":
        this.dtDelete.emit(event.data);
        break;

      default:
        break;
    }
  }

  onRowSelect(selectedRow: any) {
    if (this.enableSingleSelection) {
      if (this.selections?.id !== selectedRow.data.id) {
        this.selections = selectedRow.data;
      } else {
        this.selections = null;
      }
      this.dtSelect.emit(this.selections);
    }
    if (this.enableMultiSelection) {
      if (!this.selections[selectedRow.data.id]) {
        this.selections[selectedRow.data.id] = selectedRow.data;
      } else {
        delete this.selections[selectedRow.data.id];
      }

      const selectedRows: any[] = Object.keys(this.selections).map(
        (key: string) => {
          return this.selections[key];
        }
      );

      this.dtSelect.emit(selectedRows);
    }
  }
}

import { HttpClient, HttpParams } from "@angular/common/http";
import { getDeepFromObject } from "@nebular/auth";
import { LocalDataSource } from "ng2-smart-table";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { ResponseModel } from "../../models/response.model";

import { DashboardTableConfig, DashboardTableFilter } from "./models";

export class ModifiedServerDataSource extends LocalDataSource {
  protected conf: DashboardTableConfig;

  protected lastRequestCount: number = 0;

  constructor(conf: DashboardTableConfig) {
    super();
    this.conf = conf;

    if (!this.conf.sourceService) {
      throw new Error(
        "At least source Service must be specified as a configuration of the server data source."
      );
    }
  }

  count(): number {
    return this.lastRequestCount;
  }

  getElements(): Promise<any> {
    return this.requestElements()
      .pipe(
        map((responseData: any) => {
          this.lastRequestCount = this.extractTotalFromResponse(responseData);
          this.data = this.extractDataFromResponse(responseData);
          return this.data;
        }),
        catchError((err: any) => {
          return this.data;
        })
      )
      .toPromise();
  }

  /**
   * Extracts array of data from server response
   * @param res
   * @returns {any}
   */
  protected extractDataFromResponse(responseData: any): Array<any> {
    const data = !!this.conf.dataKey
      ? getDeepFromObject(responseData, this.conf.dataKey, [])
      : responseData;

    if (data instanceof Array) {
      return data;
    }

    throw new Error(`Data must be an array.
    Please check that data extracted from the server response by the key '${this.conf.dataKey}' exists and is array.`);
  }

  /**
   * Extracts total rows count from the server response
   * Looks for the count in the heders first, then in the response body
   * @param res
   * @returns {any}
   */
  protected extractTotalFromResponse(res: ResponseModel<any>): number {
    return getDeepFromObject(res, this.conf.totalKey, 0);
  }

  protected requestElements(): Observable<any> {
    let httpParams = this.createRequesParams();

    return this.conf.sourceService.getAll({ params: httpParams });
  }

  protected createRequesParams(): HttpParams {
    let httpParams = new HttpParams();

    if (this.conf.params) {
      Object.entries(this.conf.params).forEach((entry: [string, any]) => {
        const [param, value] = entry;
        httpParams = httpParams.set(param, String(value));
      });
    }

    httpParams = this.addSortRequestParams(httpParams);
    httpParams = this.addFilterRequestParams(httpParams);
    httpParams = this.addPagerRequestParams(httpParams);

    return httpParams;
  }

  protected addSortRequestParams(httpParams: HttpParams): HttpParams {
    if (this.sortConf) {
      this.sortConf.forEach((fieldConf) => {
        if (this.conf?.sortFieldValueMap[fieldConf.field]) {
          httpParams = httpParams.set(
            this.conf.sortFieldKey,
            this.conf.sortFieldValueMap[fieldConf.field] ?? fieldConf.field
          );
        }

        if (fieldConf.direction.toUpperCase() == "ASC")
          httpParams = httpParams.set(this.conf.sortDirKey, "0");
        else if (fieldConf.direction.toUpperCase() == "DESC")
          httpParams = httpParams.set(this.conf.sortDirKey, "1");
      });
    }

    return httpParams;
  }

  protected addFilterRequestParams(httpParams: HttpParams): HttpParams {
    const filters: DashboardTableFilter[] = this.conf.filters;
    const filterInput: any[] = this.filterConf.filters;
    const filterParams: any = {};

    if (filterInput && filterInput.length > 0) {
      filters?.forEach((filter: DashboardTableFilter) => {
        filterInput.forEach((filterInput: any) => {
          if (filterInput.search) {
            if (filterInput.field === filter.columnId) {
              filterParams[filter.key] = filterInput.search;
            }
          }
        });
      });
    }

    Object.entries(filterParams).forEach((entry: [string, string]) => {
      const [key, value] = entry;

      httpParams = httpParams.set(key, value);
    });

    return httpParams;
  }

  protected addPagerRequestParams(httpParams: HttpParams): HttpParams {
    if (
      this.pagingConf &&
      this.pagingConf["page"] &&
      this.pagingConf["perPage"]
    ) {
      httpParams = httpParams.set(
        this.conf.pagerPageKey,
        this.pagingConf["page"]
      );
      httpParams = httpParams.set(
        this.conf.pagerLimitKey,
        this.pagingConf["perPage"]
      );
    }

    return httpParams;
  }
}

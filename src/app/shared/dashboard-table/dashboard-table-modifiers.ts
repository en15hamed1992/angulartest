import { Subject } from "rxjs";

export class DashboardTableModifiers {
    private _resetFilters$: Subject<void>;
    private _refreshData$: Subject<void>;

    constructor(modifiers: { resetFilters: Subject<void>; refreshData: Subject<void> }) {
        this._resetFilters$ = modifiers.resetFilters;
        this._refreshData$ = modifiers.refreshData;
    }

    resetFilters(): void {
        this._resetFilters$.next();
    }

    refreshData(): void {
        this._refreshData$.next();
    }
}

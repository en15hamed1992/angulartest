import { DashboardTableFilter } from ".";
import { ApiServiceInterface } from "../../../interfaces/api-service.interface";

export interface DashboardTableConfig {
  sourceService: ApiServiceInterface;
  params?: any;
  dataKey?: string;
  pagerPageKey?: string;

  sortFieldKey?: string;

  sortDirKey?: string;
  sortKey?: string;

  sortFieldValueMap?: { [key: string]: string };

  pagerLimitKey?: string;
  filters?: DashboardTableFilter[];
  totalKey?: string;
}

import { Type } from "@angular/core";

export interface DashboardTableColumsFilter {
  type?: "checkbox" | "select" | "completer" | "list" | "custom";
  component?: Type<any>;
  config?: any;
}

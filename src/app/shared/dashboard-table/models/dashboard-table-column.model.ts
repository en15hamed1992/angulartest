import { Type } from "@angular/core";

import { DashboardTableColumsEditor, DashboardTableColumsFilter } from ".";

export interface DashboardTableColumn {
    title?: string;
    class?: string;
    width?: string;
    hide?: boolean;
    editable?: boolean;
    type?: "text" | "html" | "custom";
    editor?: DashboardTableColumsEditor;
    filter?: DashboardTableColumsFilter | boolean;
    sort?: boolean;
    sortDirection?: "asc" | "desc";
    permissions?: string[];
    renderComponent?: Type<any>;
    onComponentInitFunction?: (componenetInstance: Type<any>) => any;
    valuePrepareFunction?: (cell: any, row: any) => any;
    compareFunction?: (direction: any, prev: any, next: any) => any;
    filterFunction?: (cell: any, search: any) => any;
}

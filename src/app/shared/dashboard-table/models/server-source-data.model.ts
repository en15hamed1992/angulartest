import { ServerSourceDataFilter, ServerSourceDataPaging } from ".";

export interface ServerSourceData {
    action: string;
    elements: any[];
    filter: ServerSourceDataFilter;
    paging: ServerSourceDataPaging;
    sort: any[];
}

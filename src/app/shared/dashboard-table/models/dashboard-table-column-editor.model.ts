import { Type } from "@angular/core";

export interface DashboardTableColumsEditor {
    type?: "text" | "textarea" | "completer" | "list" | "checkbox" | "custom";
    component?: Type<any>;
    config?: any;
}

export interface ServerSourceDataFilter {
    andOperator: boolean;
    filters: {
        field: string;
        search: string;
        filter: any;
    }[];
}

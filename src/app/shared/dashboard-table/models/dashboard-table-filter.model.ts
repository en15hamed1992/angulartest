export interface DashboardTableFilter {
    columnId: string;
    key: string;
}

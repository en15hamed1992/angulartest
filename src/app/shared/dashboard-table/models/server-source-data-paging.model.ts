export interface ServerSourceDataPaging {
    page: number;
    perPage: number;
}

export enum DashboardTableActions {
    CREATE,
    DELETE,
    VIEW_ONE,
    UPDATE,
}

export interface DashboardTableAction {
    action: DashboardTableActions;
}

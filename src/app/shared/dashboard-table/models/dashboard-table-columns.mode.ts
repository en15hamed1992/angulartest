import { DashboardTableColumn } from ".";

export type DashboardTableColumns = { [key: string]: DashboardTableColumn };

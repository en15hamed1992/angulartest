import { Component, OnInit, Input } from "@angular/core";
import { ViewCell } from "ng2-smart-table";
import { AppHelpers } from "../../utils";

@Component({
  selector: "ngx-date-cell",
  templateUrl: "./date-cell.component.html",
  styleUrls: ["./date-cell.component.scss"],
})
export class DateCellComponent implements OnInit, ViewCell {
  @Input() value: string;
  @Input() rowData: any;

  ngOnInit() {}
}

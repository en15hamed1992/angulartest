import { DataViewField } from ".";

export type DataViewFields<TData = any> = { [ket: string]: DataViewField<TData> };

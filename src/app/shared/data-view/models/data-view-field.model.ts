import { Type } from "@angular/core";

import { DataViewFieldValueComponent } from ".";

export interface DataViewField<TData = any> {
    key?: string;
    title: string;
    fieldValuePrepareFn?: PreparationFn<TData>;
    renderComponent?: Type<DataViewFieldValueComponent<TData>>;
    permissions?: string[];
}

export type PreparationFn<TData> = (data: TData, fieldValue: any) => any;

export interface DataViewFieldValueComponent<TData = any> {
    rowData: TData;
    value: any;
}

import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { DataViewFieldComponent } from "./data-view-field.component";

describe("DataViewFieldComponent", () => {
    let component: DataViewFieldComponent;
    let fixture: ComponentFixture<DataViewFieldComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DataViewFieldComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DataViewFieldComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

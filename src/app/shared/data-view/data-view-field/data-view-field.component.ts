import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ComponentFactoryResolver,
    Input,
    OnInit,
    ViewChild,
} from "@angular/core";
import { NbLayoutDirection, NbLayoutDirectionService, NbThemeService } from "@nebular/theme";

import { ViewHostDirective } from "../directives/view-host.directive";
import { DataViewField, DataViewFieldValueComponent } from "../models";

@Component({
    selector: "ngx-data-view-field",
    templateUrl: "./data-view-field.component.html",
    styleUrls: ["./data-view-field.component.scss"],
})
export class DataViewFieldComponent implements OnInit, AfterViewInit {
    @ViewChild(ViewHostDirective, { static: false })
    ngxViewHost: ViewHostDirective;

    @Input() fieldKey: string;
    @Input() originData: any;
    @Input() viewData: any;
    @Input() viewField: DataViewField;

    direction: NbLayoutDirection;
    themeName: string;

    constructor(
        private changeDetector: ChangeDetectorRef,
        private componentFactoryResolver: ComponentFactoryResolver,
        private directionService: NbLayoutDirectionService,
        private themeService: NbThemeService
    ) {}

    ngOnInit(): void {
        this.directionService.onDirectionChange().subscribe((direction: NbLayoutDirection) => {
            this.direction = direction;
        });

        this.themeService.onThemeChange().subscribe((theme: any) => {
            this.themeName = theme.name;
        });
    }

    ngAfterViewInit(): void {
        if (this.viewField.renderComponent) {
            const viewContainerRef = this.ngxViewHost.viewContainerRef;
            viewContainerRef.clear();
            const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
                this.viewField.renderComponent
            );
            const componentRef = viewContainerRef.createComponent<DataViewFieldValueComponent>(
                componentFactory
            );

            componentRef.instance.rowData = this.originData;
            componentRef.instance.value = this.viewData[this.fieldKey];

            this.changeDetector.detectChanges();
        }
    }
}

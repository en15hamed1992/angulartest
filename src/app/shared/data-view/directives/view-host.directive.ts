import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
    selector: "[ngxViewHost]",
})
export class ViewHostDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}

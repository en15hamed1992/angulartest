import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { Observable, of } from "rxjs";
import { RoleProviderService } from "../../services/role-provider.service";

import { DataViewField, DataViewFields } from "./models";

@Component({
  selector: "data-view",
  templateUrl: "./data-view.component.html",
  styleUrls: ["./data-view.component.scss"],
})
export class DataViewComponent implements OnInit, OnChanges {
  @Input("data") data: any;
  @Input("viewFields") viewFields: DataViewFields;

  @Output("viewModelCreate") viewModelCreate: EventEmitter<any>;

  viewData: any;
  reload: boolean;

  constructor(private roleProviderService: RoleProviderService) {
    this.viewModelCreate = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.reload = false;
    this.initViewData();
    this.viewModelCreate.emit(this.viewData);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data) {
      this.data = changes.data.currentValue;

      this.reload = true;
      setTimeout(() => {
        this.reload = false;
      }, 100);
      this.initViewData();
      this.viewModelCreate.emit(this.viewData);
    }
  }

  initViewData(): void {
    const obj: any = {};

    Object.entries(this.viewFields).forEach(
      ([key, value]: [string, DataViewField]) => {
        obj[key] = value.fieldValuePrepareFn
          ? value.fieldValuePrepareFn(this.data, this.data[key])
          : this.data[key];
      }
    );

    this.viewData = obj;
  }

  isEmpty(field: any) {
    if (field === null || field === undefined || field === "") {
      return true;
    }

    return false;
  }

  isGranted(permissions: string[]): boolean | Observable<boolean> {
    if (!permissions || !permissions.length) {
      return of(true);
    }

    return this.roleProviderService.isGranted(permissions);
  }

  getEntries(obj: DataViewFields): { key: string; value: DataViewField }[] {
    return Object.entries(obj).map(([key, value]) => {
      return {
        key: key,
        value: value,
      };
    });
  }
}

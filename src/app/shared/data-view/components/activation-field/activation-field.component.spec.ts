import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ActivationFieldComponent } from "./activation-field.component";

describe("ActivationFieldComponent", () => {
    let component: ActivationFieldComponent;
    let fixture: ComponentFixture<ActivationFieldComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ActivationFieldComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ActivationFieldComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

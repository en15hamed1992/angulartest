import { Component, Input, OnInit } from "@angular/core";
import { DataViewFieldValueComponent } from "../../models";

@Component({
    selector: "ngx-activation-field",
    templateUrl: "./activation-field.component.html",
    styleUrls: ["./activation-field.component.scss"],
})
export class ActivationFieldComponent implements OnInit, DataViewFieldValueComponent<any> {
    @Input() rowData: any;
    @Input() value: string;

    constructor() {}

    ngOnInit(): void {}

    getBooleanValue(): boolean {
        return Boolean(this.value);
    }
}

import { Component, Input, OnInit } from "@angular/core";
import { DataViewFieldValueComponent } from "../../models";

@Component({
    selector: "ngx-image-field",
    templateUrl: "./image-field.component.html",
    styleUrls: ["./image-field.component.scss"],
})
export class ImageFieldComponent implements OnInit, DataViewFieldValueComponent<any> {
    @Input() rowData: any;
    @Input() value: string;

    constructor() {}

    ngOnInit(): void {
    }
}

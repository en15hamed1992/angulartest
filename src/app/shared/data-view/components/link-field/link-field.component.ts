import { Component, Input, OnInit } from "@angular/core";
import { DataViewFieldValueComponent } from "../../models";

@Component({
    selector: "ngx-link-field",
    templateUrl: "./link-field.component.html",
    styleUrls: ["./link-field.component.scss"],
})
export class LinkFieldComponent implements OnInit, DataViewFieldValueComponent<any> {
    @Input() rowData: any;
    @Input() value: string;

    constructor() {}

    ngOnInit(): void {}
}

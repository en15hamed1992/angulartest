import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageModalFieldComponent } from './image-modal-field.component';

describe('ImageModalFieldComponent', () => {
  let component: ImageModalFieldComponent;
  let fixture: ComponentFixture<ImageModalFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageModalFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageModalFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit, TemplateRef } from "@angular/core";
import { NbDialogRef, NbDialogService } from "@nebular/theme";
import { DataViewFieldValueComponent } from "../../models";

@Component({
    selector: "ngx-image-modal-field",
    templateUrl: "./image-modal-field.component.html",
    styleUrls: ["./image-modal-field.component.scss"],
})
export class ImageModalFieldComponent implements OnInit, DataViewFieldValueComponent<any> {
    @Input() rowData: any;
    @Input() value: { title: string; url: string };

    dialog: NbDialogRef<any>;

    constructor(private dialogService: NbDialogService) {}

    ngOnInit(): void {}

    open(dialog: TemplateRef<any>): void {
        this.dialog = this.dialogService.open(dialog, {
            context: "this is some additional data passed to dialog",
        });
    }

    close(): void {
        this.dialog.close();
    }
}

import { Component, Input, OnInit } from "@angular/core";
import { DataViewFieldValueComponent } from "../../models";

@Component({
    selector: "ngx-date-field",
    templateUrl: "./date-field.component.html",
    styleUrls: ["./date-field.component.scss"],
})
export class DateFieldComponent implements OnInit, DataViewFieldValueComponent<any> {
    @Input() rowData: any;
    @Input() value: string;

    constructor() {}

    ngOnInit(): void {
    }
}

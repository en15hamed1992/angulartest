import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CustomMinMaxDateFilterComponent } from "./custom-min-max-date-filter.component";

describe("CustomMinMaxDateFilterComponent", () => {
    let component: CustomMinMaxDateFilterComponent;
    let fixture: ComponentFixture<CustomMinMaxDateFilterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CustomMinMaxDateFilterComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustomMinMaxDateFilterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

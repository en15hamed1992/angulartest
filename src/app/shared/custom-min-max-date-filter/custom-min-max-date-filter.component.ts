import { Component, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { DefaultFilter } from "ng2-smart-table";

import { AppHelpers } from "../../utils";
import { NbLayoutDirectionService } from "@nebular/theme";
import { TranslateService } from "../../translation/translate.service";

@Component({
  selector: "ngx-custom-min-max-date-filter",
  templateUrl: "./custom-min-max-date-filter.component.html",
  styleUrls: ["./custom-min-max-date-filter.component.scss"],
})
export class CustomMinMaxDateFilterComponent
  extends DefaultFilter
  implements OnInit, OnChanges {
  minDateInputControl: FormControl;
  maxDateInputControl: FormControl;

  minPlaceholder: string;
  maxPlaceholder: string;

  constructor(
    private directionService: NbLayoutDirectionService,
    private translateService: TranslateService
  ) {
    super();

    this.minDateInputControl = new FormControl();
    this.maxDateInputControl = new FormControl();
  }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(() => {
      this.minPlaceholder = this.translateService.translate(
        this.column.filter.config.min.placeholder
      );
      this.maxPlaceholder = this.translateService.translate(
        this.column.filter.config.max.placeholder
      );
    });

    this.minDateInputControl.valueChanges
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe((value: number) => {
        const prevColomunId = this.column.id;
        this.column.id = this.column.filter.config.min.id;

        this.query =
          value !== null
            ? AppHelpers.dateToUtc(this.minDateInputControl.value).toString()
            : "";
        this.setFilter();

        this.column.id = prevColomunId;
      });

    this.maxDateInputControl.valueChanges
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe((value: number) => {
        console.log({ value });
        const prevColomunId = this.column.id;
        this.column.id = this.column.filter.config.max.id;

        this.query =
          value !== null
            ? AppHelpers.dateToUtc(this.maxDateInputControl.value).toString()
            : "";
            
        console.log({ query:this.query });
        this.setFilter();

        this.column.id = prevColomunId;
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.query) {
      this.query = changes.query.currentValue;
    }
  }
}

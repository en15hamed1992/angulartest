import { Component, OnInit, Input } from "@angular/core";
import { ViewCell } from "ng2-smart-table";

@Component({
    selector: "ngx-status-cell",
    templateUrl: "./status-cell.component.html",
    styleUrls: ["./status-cell.component.scss"],
})
export class StatusCellComponent implements OnInit, ViewCell {
    @Input() value: string;
    @Input() rowData: any;

    ngOnInit(): void {}
}

import { trigger, transition, style, animate } from "@angular/animations";

export const fadeInTrigger = trigger("fadeIn", [
    transition(":enter, :leave", [style({ opacity: 0 }), animate("2s")]),
]);

import { Component, OnInit, Input, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";

@Component({
    selector: "ngx-confirmation-modal",
    templateUrl: "./confirmation-modal.component.html",
    styleUrls: ["./confirmation-modal.component.scss"],
})
export class ConfirmationModalComponent implements OnInit, AfterViewInit {
    @ViewChild("cancelBtn", { static: false }) cancelBtn: {
        hostElement: ElementRef<HTMLButtonElement>;
    };
    @ViewChild("confirmBtn", { static: false }) confirmBtn: {
        hostElement: ElementRef<HTMLButtonElement>;
    };

    @Input() title: string;
    @Input() message: string;
    @Input() onConfirm: () => void;

    constructor(protected dialogRef: NbDialogRef<ConfirmationModalComponent>) {}

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        this.cancelBtn.hostElement.nativeElement.blur();
        this.confirmBtn.hostElement.nativeElement.blur();
    }

    dismiss() {
        this.dialogRef.close();
    }

    confirm(): void {
        this.onConfirm();
        this.dismiss();
    }
}

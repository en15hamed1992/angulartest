export class ConfirmationContext {
    title: string;
    message: string;
    onConfirm: () => void;
}

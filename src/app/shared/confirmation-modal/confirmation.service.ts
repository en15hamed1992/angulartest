import { Injectable } from "@angular/core";
import {
  NbComponentStatus,
  NbDialogRef,
  NbDialogService,
  NbGlobalPhysicalPosition,
  NbToastrService,
} from "@nebular/theme";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import {
  AppHelpers,
  AppOperation,
  AppOperationKeywordsMapping,
} from "../../utils";
import { LoadingService } from "../loading-modal/loading-modal.service";
import { ConfirmationModalComponent } from "./confirmation-modal.component";
import { ConfirmationContext } from "./confirmation.model";

@Injectable({
  providedIn: "root",
})
export class ConfirmationService {
  private _dialog: NbDialogRef<ConfirmationModalComponent>;
  private _isOpen = false;
  successStatus:   NbComponentStatus="danger";
  constructor(
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private loadingService: LoadingService
  ) {}

  openModal(context: ConfirmationContext): void {
    this._dialog = this.dialogService.open(ConfirmationModalComponent, {
      context: context,
    });

    this._isOpen = true;
  }
  SetStatus(status:NbComponentStatus):void{
    this.successStatus=status
  }
  closeModal(): void {
    this._dialog?.close();

    this._isOpen = false;
  }

  isModalOpen(): boolean {
    return this._isOpen;
  }

  conformOperation({
    operation,
    modelName,
    modelShowValue,
    onConfirmAction,
    afterDoneAction,
    
  }: {
    operation: AppOperation;
    modelName: string;
    modelShowValue: string;
   
    onConfirmAction: () => Observable<any>;
    afterDoneAction: () => void;
  }): void {
    const confirmKeyword = AppOperationKeywordsMapping[
      operation
    ].confirmKeyword.toLowerCase();

    const successKeyword = AppOperationKeywordsMapping[
      operation
    ].successKeyword.toLowerCase();

    const loadKeyword = AppOperationKeywordsMapping[
      operation
    ].loadingKeyword.toLowerCase();

    this.openModal({
      title: `${AppHelpers.uppercaseFirst(
        confirmKeyword
      )} ${AppHelpers.uppercaseFirst(modelName)}`,

      message: `Are you sure you want to ${confirmKeyword} ${modelName} "${modelShowValue}" ?`,

      onConfirm: () => {
        this.loadingService.openModal({
          message: `${loadKeyword} ${modelName} "${modelShowValue}"...`,
        });

        onConfirmAction()
          .pipe(
            finalize(() => {
              this.loadingService.closeModal();
            })
          )
          .subscribe(() => {
            this.toastrService.show(
              `${modelName} has been successfully ${successKeyword}`,
              `${confirmKeyword} ${modelName}`,
              {
                position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
                limit: 3,
                hasIcon: true,
                icon: "checkmark-circle-outline",
                destroyByClick: true,
                status: this.successStatus,
              }
            );
            afterDoneAction();
          });
      },
    });
  }
}

import { Component, OnInit, Input } from "@angular/core";
import { ViewCell } from "ng2-smart-table";

@Component({
    selector: "ngx-boolean-cell",
    templateUrl: "./boolean-cell.component.html",
    styleUrls: ["./boolean-cell.component.scss"],
})
export class BooleanCellComponent implements OnInit, ViewCell {
    @Input() value: string;
    @Input() rowData: any;

    icon: string;

    ngOnInit() {
        this.icon = Boolean(this.value) ? "checkmark-circle-2-outline" : "close-circle-outline";
    }
}

import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { NgxSpinnerService } from "ngx-spinner";

import { Status } from "../../@theme/constants/status";

@Component({
    selector: "ngx-loading-modal",
    templateUrl: "./loading-modal.component.html",
    styleUrls: ["./loading-modal.component.scss"],
})
export class LoadingModalComponent implements OnInit, OnDestroy {
    @Input("message") message: string;

    spinnerConfig = {
        bdColor: "transparent",
        color: Status.PRIMARY,
        type: "ball-clip-rotate",
        size: "large",
        fullScreen: true,
    };

    constructor(private ngxSpinnerService: NgxSpinnerService) {}

    ngOnInit(): void {
        this.ngxSpinnerService.show();
    }

    ngOnDestroy(): void {
        this.ngxSpinnerService.hide();
    }
}

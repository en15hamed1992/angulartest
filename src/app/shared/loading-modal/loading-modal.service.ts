import { Injectable } from "@angular/core";
import { NbDialogRef, NbDialogService } from "@nebular/theme";
import { LoadingContext } from "./loading-context.model";
import { LoadingModalComponent } from "./loading-modal.component";

@Injectable({
    providedIn: "root",
})
export class LoadingService {
    private _dialog: NbDialogRef<LoadingModalComponent>;
    private _isOpen = false;

    constructor(private dialogService: NbDialogService) {}

    openModal(context: Partial<LoadingContext>): void {
        this._dialog = this.dialogService.open(LoadingModalComponent, {
            hasBackdrop: true,
            closeOnBackdropClick: false,
            context: {
                message: context?.message || "",
            },
        });

        this._isOpen = true;
    }

    closeModal(): void {
        this._dialog?.close();

        this._isOpen = false;
    }

    changeModal(context: Partial<LoadingContext>): void {
        this._dialog.componentRef.instance.message = context?.message || "";
    }

    isModalOpen(): boolean {
        return this._isOpen;
    }
}

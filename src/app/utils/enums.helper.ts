const StringIsNumber = function(value:any){
    if(!isNaN(Number(value)))
        return value;
}  
export class EnumHelper{
 public static  EnumToArray=(enumme:object)=> {
    const arrayObjects = []  
      
    for (const [propertyKey, propertyValue] of Object.entries(enumme)) {  
      if (!Number.isNaN(Number(propertyKey))) {  
        continue;  
         }  
    arrayObjects.push( {title:propertyKey,value:propertyValue} );  
    }
    
    return arrayObjects;
  }
}
// Turn enum into array

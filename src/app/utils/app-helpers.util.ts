import * as moment from "moment";

export interface PartitionedArray<T> {
    parts: T[][];
}

export class AppHelpers {
    public static utcToDate = (utcDate: string) => {
        try {
            const localString = moment(utcDate).local().format("YYYY-MM-DD HH:mm:ss");

            return localString;
        } catch (error) {
            console.error(error);
        }
    };

    public static dateToUtc = (localDate: string) => {
        try {
            const utcString = moment.utc(localDate).format();
            return utcString;
        } catch (error) {
            console.error(error);
        }
    };

    public static uppercaseFirst = (str: string) => {
        const firstLetter = str.charAt(0).toUpperCase();
        const restLettes = str.slice(1).toLowerCase();

        return firstLetter + restLettes;
    };

    public static partitionArrary<T>(sourceArray: T[], part: number): PartitionedArray<T> {
        let partitionCount = Math.ceil(sourceArray.length / part);

        const partition: PartitionedArray<T> = { parts: [] };

        let startIndex = 0;

        while (partitionCount > 0) {
            const partArray = sourceArray.slice(startIndex, startIndex + part);

            partition.parts.push(partArray);

            partitionCount--;
            startIndex += part;
        }

        return partition;
    }
}

import { environment } from "../../environments/environment";

export enum AppKeywords {
  ACTIVATE_KEYWORD = "REMOVE",
  // DEACTIVATE_KEYWORD = "RESTORE",

  ACCEPT_KEYWORD = "ACCEPT",
  REJECT_KEYWORD = "REJECT",
  REMOVE_KEYWORD = "REMOVE",

  ACTIVATED_KEYWORD = "REMOVED",
  // DEACTIVATED_KEYWORD = "RESTORED",

  ACCEPTED_KEYWORD = "ACCEPTED",
  REJECTED_KEYWORD = "REJECTED",
  REMOVED_KEYWORD = "REMOVED",

  ACTIVATING_KEYWORD = "REMOVING",
  // DEACTIVATING_KEYWORD = "RESTORING",

  ACCEPTING_KEYWORD = "ACCEPTING",
  REJECTING_KEYWORD = "REJECTING",
  REMOVING_KEYWORD = "REMOVING",
}

export enum AppOperation {
  ACCEPT = "accept",
  REJECT = "reject",
  REMOVE = "remove",
}
export const ID_KEY = "id";

export const AppOperationKeywordsMapping = {
  
  [AppOperation.ACCEPT]: {
    confirmKeyword: AppKeywords.ACCEPT_KEYWORD,
    successKeyword: AppKeywords.ACCEPTED_KEYWORD,
    loadingKeyword: AppKeywords.ACCEPTING_KEYWORD,
  },

  [AppOperation.REJECT]: {
    confirmKeyword: AppKeywords.REJECT_KEYWORD,
    successKeyword: AppKeywords.REJECTED_KEYWORD,
    loadingKeyword: AppKeywords.REJECTING_KEYWORD,
  },

  [AppOperation.REMOVE]: {
    confirmKeyword: AppKeywords.REMOVE_KEYWORD,
    successKeyword: AppKeywords.REMOVED_KEYWORD,
    loadingKeyword: AppKeywords.REMOVING_KEYWORD,
  },
};

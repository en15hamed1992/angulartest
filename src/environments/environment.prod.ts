/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  AUTH_APP_TOKEN_KEY: "auth_app_token",
  API_BASE_URL: "https://18.220.82.15:5006",
  login_URL: "/api/Account/LogIn",
  account_URL: "/api/Users/GetUserInformations",
  refresh_Token_URL: "/api/Account/RefreshToken",
};
